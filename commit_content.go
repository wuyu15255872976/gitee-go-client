/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 删除文件
type CommitContent struct {

	Content *ContentBasic `json:"content,omitempty"`

	Commit *Commit `json:"commit,omitempty"`
}
