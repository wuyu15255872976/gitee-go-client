/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取一个公钥
type SshKey struct {

	Id string `json:"id,omitempty"`

	Key string `json:"key,omitempty"`

	Url string `json:"url,omitempty"`

	Title string `json:"title,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`
}
