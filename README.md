# Go API client for gitee_client

## Installation
Put the package under your project folder and add the following in import:
```
    "./gitee_client"
```

## Documentation for API Endpoints

All URIs are relative to *https://gitee.com/api*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ActivityApi* | [**DeleteV5UserStarredOwnerRepo**](docs/ActivityApi.md#deletev5userstarredownerrepo) | **Delete** /v5/user/starred/{owner}/{repo} | 取消 star 一个项目
*ActivityApi* | [**DeleteV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#deletev5usersubscriptionsownerrepo) | **Delete** /v5/user/subscriptions/{owner}/{repo} | 取消 watch 一个项目
*ActivityApi* | [**GetV5Events**](docs/ActivityApi.md#getv5events) | **Get** /v5/events | 获取站内所有公开动态
*ActivityApi* | [**GetV5NetworksOwnerRepoEvents**](docs/ActivityApi.md#getv5networksownerrepoevents) | **Get** /v5/networks/{owner}/{repo}/events | 列出项目的所有公开动态
*ActivityApi* | [**GetV5NotificationsMessages**](docs/ActivityApi.md#getv5notificationsmessages) | **Get** /v5/notifications/messages | 列出授权用户的所有私信
*ActivityApi* | [**GetV5NotificationsMessagesId**](docs/ActivityApi.md#getv5notificationsmessagesid) | **Get** /v5/notifications/messages/{id} | 获取一个私信
*ActivityApi* | [**GetV5NotificationsThreads**](docs/ActivityApi.md#getv5notificationsthreads) | **Get** /v5/notifications/threads | 列出授权用户的所有通知
*ActivityApi* | [**GetV5NotificationsThreadsId**](docs/ActivityApi.md#getv5notificationsthreadsid) | **Get** /v5/notifications/threads/{id} | 获取一个通知
*ActivityApi* | [**GetV5OrgsOrgEvents**](docs/ActivityApi.md#getv5orgsorgevents) | **Get** /v5/orgs/{org}/events | 列出组织的公开动态
*ActivityApi* | [**GetV5ReposOwnerRepoEvents**](docs/ActivityApi.md#getv5reposownerrepoevents) | **Get** /v5/repos/{owner}/{repo}/events | 列出项目的所有动态
*ActivityApi* | [**GetV5ReposOwnerRepoNotifications**](docs/ActivityApi.md#getv5reposownerreponotifications) | **Get** /v5/repos/{owner}/{repo}/notifications | 列出一个项目里的通知
*ActivityApi* | [**GetV5ReposOwnerRepoStargazers**](docs/ActivityApi.md#getv5reposownerrepostargazers) | **Get** /v5/repos/{owner}/{repo}/stargazers | 列出 star 了项目的用户
*ActivityApi* | [**GetV5ReposOwnerRepoSubscribers**](docs/ActivityApi.md#getv5reposownerreposubscribers) | **Get** /v5/repos/{owner}/{repo}/subscribers | 列出 watch 了项目的用户
*ActivityApi* | [**GetV5UserStarred**](docs/ActivityApi.md#getv5userstarred) | **Get** /v5/user/starred | 列出授权用户 star 了的项目
*ActivityApi* | [**GetV5UserStarredOwnerRepo**](docs/ActivityApi.md#getv5userstarredownerrepo) | **Get** /v5/user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个项目
*ActivityApi* | [**GetV5UserSubscriptions**](docs/ActivityApi.md#getv5usersubscriptions) | **Get** /v5/user/subscriptions | 列出授权用户 watch 了的项目
*ActivityApi* | [**GetV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#getv5usersubscriptionsownerrepo) | **Get** /v5/user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个项目
*ActivityApi* | [**GetV5UsersUsernameEvents**](docs/ActivityApi.md#getv5usersusernameevents) | **Get** /v5/users/{username}/events | 列出用户的动态
*ActivityApi* | [**GetV5UsersUsernameEventsOrgsOrg**](docs/ActivityApi.md#getv5usersusernameeventsorgsorg) | **Get** /v5/users/{username}/events/orgs/{org} | 列出用户所属组织的动态
*ActivityApi* | [**GetV5UsersUsernameEventsPublic**](docs/ActivityApi.md#getv5usersusernameeventspublic) | **Get** /v5/users/{username}/events/public | 列出用户的公开动态
*ActivityApi* | [**GetV5UsersUsernameReceivedEvents**](docs/ActivityApi.md#getv5usersusernamereceivedevents) | **Get** /v5/users/{username}/received_events | 列出一个用户收到的动态
*ActivityApi* | [**GetV5UsersUsernameReceivedEventsPublic**](docs/ActivityApi.md#getv5usersusernamereceivedeventspublic) | **Get** /v5/users/{username}/received_events/public | 列出一个用户收到的公开动态
*ActivityApi* | [**GetV5UsersUsernameStarred**](docs/ActivityApi.md#getv5usersusernamestarred) | **Get** /v5/users/{username}/starred | 列出用户 star 了的项目
*ActivityApi* | [**GetV5UsersUsernameSubscriptions**](docs/ActivityApi.md#getv5usersusernamesubscriptions) | **Get** /v5/users/{username}/subscriptions | 列出用户 watch 了的项目
*ActivityApi* | [**PatchV5NotificationsMessagesId**](docs/ActivityApi.md#patchv5notificationsmessagesid) | **Patch** /v5/notifications/messages/{id} | 标记一个私信为已读
*ActivityApi* | [**PatchV5NotificationsThreadsId**](docs/ActivityApi.md#patchv5notificationsthreadsid) | **Patch** /v5/notifications/threads/{id} | 标记一个通知为已读
*ActivityApi* | [**PostV5NotificationsMessages**](docs/ActivityApi.md#postv5notificationsmessages) | **Post** /v5/notifications/messages | 发送私信给指定用户
*ActivityApi* | [**PutV5NotificationsMessages**](docs/ActivityApi.md#putv5notificationsmessages) | **Put** /v5/notifications/messages | 标记所有私信为已读
*ActivityApi* | [**PutV5NotificationsThreads**](docs/ActivityApi.md#putv5notificationsthreads) | **Put** /v5/notifications/threads | 标记所有通知为已读
*ActivityApi* | [**PutV5ReposOwnerRepoNotifications**](docs/ActivityApi.md#putv5reposownerreponotifications) | **Put** /v5/repos/{owner}/{repo}/notifications | 标记一个项目里的通知为已读
*ActivityApi* | [**PutV5UserStarredOwnerRepo**](docs/ActivityApi.md#putv5userstarredownerrepo) | **Put** /v5/user/starred/{owner}/{repo} | star 一个项目
*ActivityApi* | [**PutV5UserSubscriptionsOwnerRepo**](docs/ActivityApi.md#putv5usersubscriptionsownerrepo) | **Put** /v5/user/subscriptions/{owner}/{repo} | watch 一个项目
*GistsApi* | [**DeleteV5GistsGistIdCommentsId**](docs/GistsApi.md#deletev5gistsgistidcommentsid) | **Delete** /v5/gists/{gist_id}/comments/{id} | 删除代码片段的评论
*GistsApi* | [**DeleteV5GistsId**](docs/GistsApi.md#deletev5gistsid) | **Delete** /v5/gists/{id} | 删除该条代码片段
*GistsApi* | [**DeleteV5GistsIdStar**](docs/GistsApi.md#deletev5gistsidstar) | **Delete** /v5/gists/{id}/star | 取消Star代码片段
*GistsApi* | [**GetV5Gists**](docs/GistsApi.md#getv5gists) | **Get** /v5/gists | 获取代码片段
*GistsApi* | [**GetV5GistsGistIdComments**](docs/GistsApi.md#getv5gistsgistidcomments) | **Get** /v5/gists/{gist_id}/comments | 获取代码片段的评论
*GistsApi* | [**GetV5GistsGistIdCommentsId**](docs/GistsApi.md#getv5gistsgistidcommentsid) | **Get** /v5/gists/{gist_id}/comments/{id} | 获取单条代码片段的评论
*GistsApi* | [**GetV5GistsId**](docs/GistsApi.md#getv5gistsid) | **Get** /v5/gists/{id} | 获取单条代码片段
*GistsApi* | [**GetV5GistsIdCommits**](docs/GistsApi.md#getv5gistsidcommits) | **Get** /v5/gists/{id}/commits | 获取代码片段的commit
*GistsApi* | [**GetV5GistsIdForks**](docs/GistsApi.md#getv5gistsidforks) | **Get** /v5/gists/{id}/forks | 获取Fork该条代码片段的列表
*GistsApi* | [**GetV5GistsIdStar**](docs/GistsApi.md#getv5gistsidstar) | **Get** /v5/gists/{id}/star | 判断代码片段是否已Star
*GistsApi* | [**GetV5GistsPublic**](docs/GistsApi.md#getv5gistspublic) | **Get** /v5/gists/public | 获取公开的代码片段
*GistsApi* | [**GetV5GistsStarred**](docs/GistsApi.md#getv5gistsstarred) | **Get** /v5/gists/starred | 获取用户Star的代码片段
*GistsApi* | [**GetV5UsersUsernameGists**](docs/GistsApi.md#getv5usersusernamegists) | **Get** /v5/users/{username}/gists | 获取指定用户的公开代码片段
*GistsApi* | [**PatchV5GistsGistIdCommentsId**](docs/GistsApi.md#patchv5gistsgistidcommentsid) | **Patch** /v5/gists/{gist_id}/comments/{id} | 修改代码片段的评论
*GistsApi* | [**PatchV5GistsId**](docs/GistsApi.md#patchv5gistsid) | **Patch** /v5/gists/{id} | 修改代码片段
*GistsApi* | [**PostV5Gists**](docs/GistsApi.md#postv5gists) | **Post** /v5/gists | 创建代码片段
*GistsApi* | [**PostV5GistsGistIdComments**](docs/GistsApi.md#postv5gistsgistidcomments) | **Post** /v5/gists/{gist_id}/comments | 增加代码片段的评论
*GistsApi* | [**PostV5GistsIdForks**](docs/GistsApi.md#postv5gistsidforks) | **Post** /v5/gists/{id}/forks | Fork代码片段
*GistsApi* | [**PutV5GistsIdStar**](docs/GistsApi.md#putv5gistsidstar) | **Put** /v5/gists/{id}/star | Star代码片段
*GitDataApi* | [**GetV5ReposOwnerRepoGitBlobsSha**](docs/GitDataApi.md#getv5reposownerrepogitblobssha) | **Get** /v5/repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
*GitDataApi* | [**GetV5ReposOwnerRepoGitTreesSha**](docs/GitDataApi.md#getv5reposownerrepogittreessha) | **Get** /v5/repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree
*IssuesApi* | [**DeleteV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#deletev5reposownerrepoissuescommentsid) | **Delete** /v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
*IssuesApi* | [**GetV5Issues**](docs/IssuesApi.md#getv5issues) | **Get** /v5/issues | 获取当前授权用户的所有Issue
*IssuesApi* | [**GetV5OrgsOrgIssues**](docs/IssuesApi.md#getv5orgsorgissues) | **Get** /v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
*IssuesApi* | [**GetV5ReposOwnerRepoIssues**](docs/IssuesApi.md#getv5reposownerrepoissues) | **Get** /v5/repos/{owner}/{repo}/issues | 项目的所有Issues
*IssuesApi* | [**GetV5ReposOwnerRepoIssuesComments**](docs/IssuesApi.md#getv5reposownerrepoissuescomments) | **Get** /v5/repos/{owner}/{repo}/issues/comments | 获取项目所有Issue的评论
*IssuesApi* | [**GetV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#getv5reposownerrepoissuescommentsid) | **Get** /v5/repos/{owner}/{repo}/issues/comments/{id} | 获取项目Issue某条评论
*IssuesApi* | [**GetV5ReposOwnerRepoIssuesNumber**](docs/IssuesApi.md#getv5reposownerrepoissuesnumber) | **Get** /v5/repos/{owner}/{repo}/issues/{number} | 项目的某个Issue
*IssuesApi* | [**GetV5ReposOwnerRepoIssuesNumberComments**](docs/IssuesApi.md#getv5reposownerrepoissuesnumbercomments) | **Get** /v5/repos/{owner}/{repo}/issues/{number}/comments | 获取项目某个Issue所有的评论
*IssuesApi* | [**GetV5UserIssues**](docs/IssuesApi.md#getv5userissues) | **Get** /v5/user/issues | 获取当前授权用户的所有Issues
*IssuesApi* | [**PatchV5ReposOwnerRepoIssuesCommentsId**](docs/IssuesApi.md#patchv5reposownerrepoissuescommentsid) | **Patch** /v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
*IssuesApi* | [**PatchV5ReposOwnerRepoIssuesNumber**](docs/IssuesApi.md#patchv5reposownerrepoissuesnumber) | **Patch** /v5/repos/{owner}/{repo}/issues/{number} | 更新Issue
*IssuesApi* | [**PostV5ReposOwnerRepoIssues**](docs/IssuesApi.md#postv5reposownerrepoissues) | **Post** /v5/repos/{owner}/{repo}/issues | 创建Issue
*IssuesApi* | [**PostV5ReposOwnerRepoIssuesNumberComments**](docs/IssuesApi.md#postv5reposownerrepoissuesnumbercomments) | **Post** /v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论
*LabelsApi* | [**DeleteV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#deletev5reposownerrepoissuesnumberlabels) | **Delete** /v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
*LabelsApi* | [**DeleteV5ReposOwnerRepoIssuesNumberLabelsName**](docs/LabelsApi.md#deletev5reposownerrepoissuesnumberlabelsname) | **Delete** /v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
*LabelsApi* | [**DeleteV5ReposOwnerRepoLabelsName**](docs/LabelsApi.md#deletev5reposownerrepolabelsname) | **Delete** /v5/repos/{owner}/{repo}/labels/{name} | 删除一个项目标签
*LabelsApi* | [**GetV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#getv5reposownerrepoissuesnumberlabels) | **Get** /v5/repos/{owner}/{repo}/issues/{number}/labels | 获取项目Issue的所有标签
*LabelsApi* | [**GetV5ReposOwnerRepoLabels**](docs/LabelsApi.md#getv5reposownerrepolabels) | **Get** /v5/repos/{owner}/{repo}/labels | 获取项目所有标签
*LabelsApi* | [**GetV5ReposOwnerRepoLabelsName**](docs/LabelsApi.md#getv5reposownerrepolabelsname) | **Get** /v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
*LabelsApi* | [**PatchV5ReposOwnerRepoLabelsOriginalName**](docs/LabelsApi.md#patchv5reposownerrepolabelsoriginalname) | **Patch** /v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个项目标签
*LabelsApi* | [**PostV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#postv5reposownerrepoissuesnumberlabels) | **Post** /v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
*LabelsApi* | [**PostV5ReposOwnerRepoLabels**](docs/LabelsApi.md#postv5reposownerrepolabels) | **Post** /v5/repos/{owner}/{repo}/labels | 创建项目标签
*LabelsApi* | [**PutV5ReposOwnerRepoIssuesNumberLabels**](docs/LabelsApi.md#putv5reposownerrepoissuesnumberlabels) | **Put** /v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签
*MilestonesApi* | [**DeleteV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#deletev5reposownerrepomilestonesnumber) | **Delete** /v5/repos/{owner}/{repo}/milestones/{number} | 删除项目单个里程碑
*MilestonesApi* | [**GetV5ReposOwnerRepoMilestones**](docs/MilestonesApi.md#getv5reposownerrepomilestones) | **Get** /v5/repos/{owner}/{repo}/milestones | 获取项目所有里程碑
*MilestonesApi* | [**GetV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#getv5reposownerrepomilestonesnumber) | **Get** /v5/repos/{owner}/{repo}/milestones/{number} | 获取项目单个里程碑
*MilestonesApi* | [**PatchV5ReposOwnerRepoMilestonesNumber**](docs/MilestonesApi.md#patchv5reposownerrepomilestonesnumber) | **Patch** /v5/repos/{owner}/{repo}/milestones/{number} | 更新项目里程碑
*MilestonesApi* | [**PostV5ReposOwnerRepoMilestones**](docs/MilestonesApi.md#postv5reposownerrepomilestones) | **Post** /v5/repos/{owner}/{repo}/milestones | 创建项目里程碑
*MiscellaneousApi* | [**GetV5Emojis**](docs/MiscellaneousApi.md#getv5emojis) | **Get** /v5/emojis | 列出可使用的 Emoji
*MiscellaneousApi* | [**GetV5GitignoreTemplates**](docs/MiscellaneousApi.md#getv5gitignoretemplates) | **Get** /v5/gitignore/templates | 列出可使用的 .gitignore 模板
*MiscellaneousApi* | [**GetV5GitignoreTemplatesName**](docs/MiscellaneousApi.md#getv5gitignoretemplatesname) | **Get** /v5/gitignore/templates/{name} | 获取一个 .gitignore 模板
*MiscellaneousApi* | [**GetV5GitignoreTemplatesNameRaw**](docs/MiscellaneousApi.md#getv5gitignoretemplatesnameraw) | **Get** /v5/gitignore/templates/{name}/raw | 获取一个 .gitignore 模板原始文件
*MiscellaneousApi* | [**GetV5Licenses**](docs/MiscellaneousApi.md#getv5licenses) | **Get** /v5/licenses | 列出可使用的开源许可协议
*MiscellaneousApi* | [**GetV5LicensesLicense**](docs/MiscellaneousApi.md#getv5licenseslicense) | **Get** /v5/licenses/{license} | 获取一个开源许可协议
*MiscellaneousApi* | [**GetV5LicensesLicenseRaw**](docs/MiscellaneousApi.md#getv5licenseslicenseraw) | **Get** /v5/licenses/{license}/raw | 获取一个开源许可协议原始文件
*MiscellaneousApi* | [**GetV5ReposOwnerRepoLicense**](docs/MiscellaneousApi.md#getv5reposownerrepolicense) | **Get** /v5/repos/{owner}/{repo}/license | 获取一个项目使用的开源许可协议
*MiscellaneousApi* | [**PostV5Markdown**](docs/MiscellaneousApi.md#postv5markdown) | **Post** /v5/markdown | 渲染 Markdown 文本
*OrganizationsApi* | [**DeleteV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#deletev5orgsorgmembershipsusername) | **Delete** /v5/orgs/{org}/memberships/{username} | 移除授权用户所管理组织中的成员
*OrganizationsApi* | [**DeleteV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#deletev5usermembershipsorgsorg) | **Delete** /v5/user/memberships/orgs/{org} | 退出一个组织
*OrganizationsApi* | [**GetV5OrgsOrg**](docs/OrganizationsApi.md#getv5orgsorg) | **Get** /v5/orgs/{org} | 获取一个组织
*OrganizationsApi* | [**GetV5OrgsOrgMembers**](docs/OrganizationsApi.md#getv5orgsorgmembers) | **Get** /v5/orgs/{org}/members | 列出一个组织的所有成员
*OrganizationsApi* | [**GetV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#getv5orgsorgmembershipsusername) | **Get** /v5/orgs/{org}/memberships/{username} | 获取授权用户所属组织的一个成员
*OrganizationsApi* | [**GetV5UserMembershipsOrgs**](docs/OrganizationsApi.md#getv5usermembershipsorgs) | **Get** /v5/user/memberships/orgs | 列出授权用户在所属组织的成员资料
*OrganizationsApi* | [**GetV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#getv5usermembershipsorgsorg) | **Get** /v5/user/memberships/orgs/{org} | 获取授权用户在一个组织的成员资料
*OrganizationsApi* | [**GetV5UserOrgs**](docs/OrganizationsApi.md#getv5userorgs) | **Get** /v5/user/orgs | 列出授权用户所属的组织
*OrganizationsApi* | [**GetV5UsersUsernameOrgs**](docs/OrganizationsApi.md#getv5usersusernameorgs) | **Get** /v5/users/{username}/orgs | 列出用户所属的组织
*OrganizationsApi* | [**PatchV5OrgsOrg**](docs/OrganizationsApi.md#patchv5orgsorg) | **Patch** /v5/orgs/{org} | 更新授权用户所管理的组织资料
*OrganizationsApi* | [**PatchV5UserMembershipsOrgsOrg**](docs/OrganizationsApi.md#patchv5usermembershipsorgsorg) | **Patch** /v5/user/memberships/orgs/{org} | 更新授权用户在一个组织的成员资料
*OrganizationsApi* | [**PutV5OrgsOrgMembershipsUsername**](docs/OrganizationsApi.md#putv5orgsorgmembershipsusername) | **Put** /v5/orgs/{org}/memberships/{username} | 增加或更新授权用户所管理组织的成员
*PullRequestsApi* | [**DeleteV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#deletev5reposownerrepopullscommentsid) | **Delete** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
*PullRequestsApi* | [**DeleteV5ReposOwnerRepoPullsNumberRequestedReviewers**](docs/PullRequestsApi.md#deletev5reposownerrepopullsnumberrequestedreviewers) | **Delete** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 移除审查人员
*PullRequestsApi* | [**GetV5ReposOwnerRepoPulls**](docs/PullRequestsApi.md#getv5reposownerrepopulls) | **Get** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsComments**](docs/PullRequestsApi.md#getv5reposownerrepopullscomments) | **Get** /v5/repos/{owner}/{repo}/pulls/comments | 获取该项目下的所有Pull Request评论
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#getv5reposownerrepopullscommentsid) | **Get** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumber**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumber) | **Get** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumberComments**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumbercomments) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumberCommits**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumbercommits) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumberFiles**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumberfiles) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumberMerge**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumbermerge) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
*PullRequestsApi* | [**GetV5ReposOwnerRepoPullsNumberRequestedReviewers**](docs/PullRequestsApi.md#getv5reposownerrepopullsnumberrequestedreviewers) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 获取审查人员的列表
*PullRequestsApi* | [**PatchV5ReposOwnerRepoPullsCommentsId**](docs/PullRequestsApi.md#patchv5reposownerrepopullscommentsid) | **Patch** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
*PullRequestsApi* | [**PatchV5ReposOwnerRepoPullsNumber**](docs/PullRequestsApi.md#patchv5reposownerrepopullsnumber) | **Patch** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
*PullRequestsApi* | [**PostV5ReposOwnerRepoPulls**](docs/PullRequestsApi.md#postv5reposownerrepopulls) | **Post** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
*PullRequestsApi* | [**PostV5ReposOwnerRepoPullsNumberComments**](docs/PullRequestsApi.md#postv5reposownerrepopullsnumbercomments) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
*PullRequestsApi* | [**PostV5ReposOwnerRepoPullsNumberRequestedReviewers**](docs/PullRequestsApi.md#postv5reposownerrepopullsnumberrequestedreviewers) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 增加审查人员
*PullRequestsApi* | [**PutV5ReposOwnerRepoPullsNumberMerge**](docs/PullRequestsApi.md#putv5reposownerrepopullsnumbermerge) | **Put** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request
*RepositoriesApi* | [**DeleteV5ReposOwnerRepo**](docs/RepositoriesApi.md#deletev5reposownerrepo) | **Delete** /v5/repos/{owner}/{repo} | 删除一个项目
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoBranchesBranchProtection**](docs/RepositoriesApi.md#deletev5reposownerrepobranchesbranchprotection) | **Delete** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#deletev5reposownerrepocollaboratorsusername) | **Delete** /v5/repos/{owner}/{repo}/collaborators/{username} | 移除项目成员
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#deletev5reposownerrepocommentsid) | **Delete** /v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#deletev5reposownerrepocontentspath) | **Delete** /v5/repos/{owner}/{repo}/contents/{path} | 删除文件
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoKeysId**](docs/RepositoriesApi.md#deletev5reposownerrepokeysid) | **Delete** /v5/repos/{owner}/{repo}/keys/{id} | 删除一个项目公钥
*RepositoriesApi* | [**DeleteV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#deletev5reposownerreporeleasesid) | **Delete** /v5/repos/{owner}/{repo}/releases/{id} | 删除项目Release
*RepositoriesApi* | [**GetV5OrgsOrgRepos**](docs/RepositoriesApi.md#getv5orgsorgrepos) | **Get** /v5/orgs/{org}/repos | 获取一个组织的项目
*RepositoriesApi* | [**GetV5ReposOwnerRepo**](docs/RepositoriesApi.md#getv5reposownerrepo) | **Get** /v5/repos/{owner}/{repo} | 列出授权用户的某个项目
*RepositoriesApi* | [**GetV5ReposOwnerRepoBranches**](docs/RepositoriesApi.md#getv5reposownerrepobranches) | **Get** /v5/repos/{owner}/{repo}/branches | 获取所有分支
*RepositoriesApi* | [**GetV5ReposOwnerRepoBranchesBranch**](docs/RepositoriesApi.md#getv5reposownerrepobranchesbranch) | **Get** /v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
*RepositoriesApi* | [**GetV5ReposOwnerRepoCollaborators**](docs/RepositoriesApi.md#getv5reposownerrepocollaborators) | **Get** /v5/repos/{owner}/{repo}/collaborators | 获取项目的所有成员
*RepositoriesApi* | [**GetV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#getv5reposownerrepocollaboratorsusername) | **Get** /v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为项目成员
*RepositoriesApi* | [**GetV5ReposOwnerRepoCollaboratorsUsernamePermission**](docs/RepositoriesApi.md#getv5reposownerrepocollaboratorsusernamepermission) | **Get** /v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看项目成员的权限
*RepositoriesApi* | [**GetV5ReposOwnerRepoComments**](docs/RepositoriesApi.md#getv5reposownerrepocomments) | **Get** /v5/repos/{owner}/{repo}/comments | 获取项目的Commit评论
*RepositoriesApi* | [**GetV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#getv5reposownerrepocommentsid) | **Get** /v5/repos/{owner}/{repo}/comments/{id} | 获取项目的某条Commit评论
*RepositoriesApi* | [**GetV5ReposOwnerRepoCommits**](docs/RepositoriesApi.md#getv5reposownerrepocommits) | **Get** /v5/repos/{owner}/{repo}/commits | 项目的所有提交
*RepositoriesApi* | [**GetV5ReposOwnerRepoCommitsRefComments**](docs/RepositoriesApi.md#getv5reposownerrepocommitsrefcomments) | **Get** /v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
*RepositoriesApi* | [**GetV5ReposOwnerRepoCommitsSha**](docs/RepositoriesApi.md#getv5reposownerrepocommitssha) | **Get** /v5/repos/{owner}/{repo}/commits/{sha} | 项目的某个提交
*RepositoriesApi* | [**GetV5ReposOwnerRepoCompareBaseHead**](docs/RepositoriesApi.md#getv5reposownerrepocomparebasehead) | **Get** /v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
*RepositoriesApi* | [**GetV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#getv5reposownerrepocontentspath) | **Get** /v5/repos/{owner}/{repo}/contents(/{path}) | 获取仓库具体路径下的内容
*RepositoriesApi* | [**GetV5ReposOwnerRepoContributors**](docs/RepositoriesApi.md#getv5reposownerrepocontributors) | **Get** /v5/repos/{owner}/{repo}/contributors | 获取项目贡献者
*RepositoriesApi* | [**GetV5ReposOwnerRepoForks**](docs/RepositoriesApi.md#getv5reposownerrepoforks) | **Get** /v5/repos/{owner}/{repo}/forks | 查看项目的Forks
*RepositoriesApi* | [**GetV5ReposOwnerRepoKeys**](docs/RepositoriesApi.md#getv5reposownerrepokeys) | **Get** /v5/repos/{owner}/{repo}/keys | 展示项目的公钥
*RepositoriesApi* | [**GetV5ReposOwnerRepoKeysId**](docs/RepositoriesApi.md#getv5reposownerrepokeysid) | **Get** /v5/repos/{owner}/{repo}/keys/{id} | 获取项目的单个公钥
*RepositoriesApi* | [**GetV5ReposOwnerRepoPages**](docs/RepositoriesApi.md#getv5reposownerrepopages) | **Get** /v5/repos/{owner}/{repo}/pages | 获取Pages信息
*RepositoriesApi* | [**GetV5ReposOwnerRepoReadme**](docs/RepositoriesApi.md#getv5reposownerreporeadme) | **Get** /v5/repos/{owner}/{repo}/readme | 获取仓库README
*RepositoriesApi* | [**GetV5ReposOwnerRepoReleases**](docs/RepositoriesApi.md#getv5reposownerreporeleases) | **Get** /v5/repos/{owner}/{repo}/releases | 获取项目的所有Releases
*RepositoriesApi* | [**GetV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#getv5reposownerreporeleasesid) | **Get** /v5/repos/{owner}/{repo}/releases/{id} | 获取项目的单个Releases
*RepositoriesApi* | [**GetV5ReposOwnerRepoReleasesLatest**](docs/RepositoriesApi.md#getv5reposownerreporeleaseslatest) | **Get** /v5/repos/{owner}/{repo}/releases/latest | 获取项目的最后更新的Release
*RepositoriesApi* | [**GetV5ReposOwnerRepoReleasesTagsTag**](docs/RepositoriesApi.md#getv5reposownerreporeleasestagstag) | **Get** /v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取项目的Release
*RepositoriesApi* | [**GetV5ReposOwnerRepoTags**](docs/RepositoriesApi.md#getv5reposownerrepotags) | **Get** /v5/repos/{owner}/{repo}/tags | 列出项目所有的tags
*RepositoriesApi* | [**GetV5UserRepos**](docs/RepositoriesApi.md#getv5userrepos) | **Get** /v5/user/repos | 列出授权用户的所有项目
*RepositoriesApi* | [**GetV5UsersUsernameRepos**](docs/RepositoriesApi.md#getv5usersusernamerepos) | **Get** /v5/users/{username}/repos | 获取某个用户的公开项目
*RepositoriesApi* | [**PatchV5ReposOwnerRepo**](docs/RepositoriesApi.md#patchv5reposownerrepo) | **Patch** /v5/repos/{owner}/{repo} | 更新项目设置
*RepositoriesApi* | [**PatchV5ReposOwnerRepoCommentsId**](docs/RepositoriesApi.md#patchv5reposownerrepocommentsid) | **Patch** /v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
*RepositoriesApi* | [**PatchV5ReposOwnerRepoReleasesId**](docs/RepositoriesApi.md#patchv5reposownerreporeleasesid) | **Patch** /v5/repos/{owner}/{repo}/releases/{id} | 更新项目Release
*RepositoriesApi* | [**PostV5OrgsOrgRepos**](docs/RepositoriesApi.md#postv5orgsorgrepos) | **Post** /v5/orgs/{org}/repos | 创建组织项目
*RepositoriesApi* | [**PostV5ReposOwnerRepoCommitsShaComments**](docs/RepositoriesApi.md#postv5reposownerrepocommitsshacomments) | **Post** /v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
*RepositoriesApi* | [**PostV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#postv5reposownerrepocontentspath) | **Post** /v5/repos/{owner}/{repo}/contents/{path} | 新建文件
*RepositoriesApi* | [**PostV5ReposOwnerRepoForks**](docs/RepositoriesApi.md#postv5reposownerrepoforks) | **Post** /v5/repos/{owner}/{repo}/forks | Fork一个项目
*RepositoriesApi* | [**PostV5ReposOwnerRepoKeys**](docs/RepositoriesApi.md#postv5reposownerrepokeys) | **Post** /v5/repos/{owner}/{repo}/keys | 为项目添加公钥
*RepositoriesApi* | [**PostV5ReposOwnerRepoPagesBuilds**](docs/RepositoriesApi.md#postv5reposownerrepopagesbuilds) | **Post** /v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
*RepositoriesApi* | [**PostV5ReposOwnerRepoReleases**](docs/RepositoriesApi.md#postv5reposownerreporeleases) | **Post** /v5/repos/{owner}/{repo}/releases | 创建项目Release
*RepositoriesApi* | [**PostV5UserRepos**](docs/RepositoriesApi.md#postv5userrepos) | **Post** /v5/user/repos | 创建一个项目
*RepositoriesApi* | [**PutV5ReposOwnerRepoBranchesBranchProtection**](docs/RepositoriesApi.md#putv5reposownerrepobranchesbranchprotection) | **Put** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
*RepositoriesApi* | [**PutV5ReposOwnerRepoCollaboratorsUsername**](docs/RepositoriesApi.md#putv5reposownerrepocollaboratorsusername) | **Put** /v5/repos/{owner}/{repo}/collaborators/{username} | 添加项目成员
*RepositoriesApi* | [**PutV5ReposOwnerRepoContentsPath**](docs/RepositoriesApi.md#putv5reposownerrepocontentspath) | **Put** /v5/repos/{owner}/{repo}/contents/{path} | 更新文件
*UsersApi* | [**DeleteV5UserFollowingUsername**](docs/UsersApi.md#deletev5userfollowingusername) | **Delete** /v5/user/following/{username} | 取消关注一个用户
*UsersApi* | [**DeleteV5UserKeysId**](docs/UsersApi.md#deletev5userkeysid) | **Delete** /v5/user/keys/{id} | 删除一个公钥
*UsersApi* | [**DeleteV5UserUnconfirmedEmail**](docs/UsersApi.md#deletev5userunconfirmedemail) | **Delete** /v5/user/unconfirmed_email | 删除授权用户未激活的邮箱地址
*UsersApi* | [**GetV5User**](docs/UsersApi.md#getv5user) | **Get** /v5/user | 获取授权用户的资料
*UsersApi* | [**GetV5UserAddress**](docs/UsersApi.md#getv5useraddress) | **Get** /v5/user/address | 获取授权用户的地理信息
*UsersApi* | [**GetV5UserEmails**](docs/UsersApi.md#getv5useremails) | **Get** /v5/user/emails | 获取授权用户的邮箱地址
*UsersApi* | [**GetV5UserFollowers**](docs/UsersApi.md#getv5userfollowers) | **Get** /v5/user/followers | 列出授权用户的关注者
*UsersApi* | [**GetV5UserFollowing**](docs/UsersApi.md#getv5userfollowing) | **Get** /v5/user/following | 列出授权用户正关注的用户
*UsersApi* | [**GetV5UserFollowingUsername**](docs/UsersApi.md#getv5userfollowingusername) | **Get** /v5/user/following/{username} | 检查授权用户是否关注了一个用户
*UsersApi* | [**GetV5UserKeys**](docs/UsersApi.md#getv5userkeys) | **Get** /v5/user/keys | 列出授权用户的所有公钥
*UsersApi* | [**GetV5UserKeysId**](docs/UsersApi.md#getv5userkeysid) | **Get** /v5/user/keys/{id} | 获取一个公钥
*UsersApi* | [**GetV5UsersUsername**](docs/UsersApi.md#getv5usersusername) | **Get** /v5/users/{username} | 获取一个用户
*UsersApi* | [**GetV5UsersUsernameFollowers**](docs/UsersApi.md#getv5usersusernamefollowers) | **Get** /v5/users/{username}/followers | 列出指定用户的关注者
*UsersApi* | [**GetV5UsersUsernameFollowing**](docs/UsersApi.md#getv5usersusernamefollowing) | **Get** /v5/users/{username}/following | 列出指定用户正在关注的用户
*UsersApi* | [**GetV5UsersUsernameFollowingTargetUser**](docs/UsersApi.md#getv5usersusernamefollowingtargetuser) | **Get** /v5/users/{username}/following/{target_user} | 检查指定用户是否关注目标用户
*UsersApi* | [**GetV5UsersUsernameKeys**](docs/UsersApi.md#getv5usersusernamekeys) | **Get** /v5/users/{username}/keys | 列出指定用户的所有公钥
*UsersApi* | [**PatchV5User**](docs/UsersApi.md#patchv5user) | **Patch** /v5/user | 更新授权用户的资料
*UsersApi* | [**PatchV5UserAddress**](docs/UsersApi.md#patchv5useraddress) | **Patch** /v5/user/address | 更新授权用户的地理信息
*UsersApi* | [**PostV5UserEmails**](docs/UsersApi.md#postv5useremails) | **Post** /v5/user/emails | 添加授权用户的新邮箱地址
*UsersApi* | [**PostV5UserKeys**](docs/UsersApi.md#postv5userkeys) | **Post** /v5/user/keys | 添加一个公钥
*UsersApi* | [**PutV5UserFollowingUsername**](docs/UsersApi.md#putv5userfollowingusername) | **Put** /v5/user/following/{username} | 关注一个用户
*WebhooksApi* | [**DeleteV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#deletev5reposownerrepohooksid) | **Delete** /v5/repos/{owner}/{repo}/hooks/{id} | 删除一个项目WebHook
*WebhooksApi* | [**GetV5ReposOwnerRepoHooks**](docs/WebhooksApi.md#getv5reposownerrepohooks) | **Get** /v5/repos/{owner}/{repo}/hooks | 列出项目的WebHooks
*WebhooksApi* | [**GetV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#getv5reposownerrepohooksid) | **Get** /v5/repos/{owner}/{repo}/hooks/{id} | 获取项目单个WebHook
*WebhooksApi* | [**PatchV5ReposOwnerRepoHooksId**](docs/WebhooksApi.md#patchv5reposownerrepohooksid) | **Patch** /v5/repos/{owner}/{repo}/hooks/{id} | 更新一个项目WebHook
*WebhooksApi* | [**PostV5ReposOwnerRepoHooks**](docs/WebhooksApi.md#postv5reposownerrepohooks) | **Post** /v5/repos/{owner}/{repo}/hooks | 创建一个项目WebHook
*WebhooksApi* | [**PostV5ReposOwnerRepoHooksIdTests**](docs/WebhooksApi.md#postv5reposownerrepohooksidtests) | **Post** /v5/repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功


## Documentation For Models

 - [Blob](docs/Blob.md)
 - [Branch](docs/Branch.md)
 - [Code](docs/Code.md)
 - [CodeComment](docs/CodeComment.md)
 - [CodeForks](docs/CodeForks.md)
 - [CodeForksHistory](docs/CodeForksHistory.md)
 - [Commit](docs/Commit.md)
 - [CommitContent](docs/CommitContent.md)
 - [Compare](docs/Compare.md)
 - [CompleteBranch](docs/CompleteBranch.md)
 - [Content](docs/Content.md)
 - [ContentBasic](docs/ContentBasic.md)
 - [Event](docs/Event.md)
 - [Group](docs/Group.md)
 - [GroupDetail](docs/GroupDetail.md)
 - [GroupMember](docs/GroupMember.md)
 - [Hook](docs/Hook.md)
 - [Issue](docs/Issue.md)
 - [Label](docs/Label.md)
 - [Milestone](docs/Milestone.md)
 - [Project](docs/Project.md)
 - [ProjectBasic](docs/ProjectBasic.md)
 - [PullRequest](docs/PullRequest.md)
 - [PullRequestComments](docs/PullRequestComments.md)
 - [PullRequestCommits](docs/PullRequestCommits.md)
 - [PullRequestFiles](docs/PullRequestFiles.md)
 - [Release](docs/Release.md)
 - [RepoCommit](docs/RepoCommit.md)
 - [SshKey](docs/SshKey.md)
 - [SshKeyBasic](docs/SshKeyBasic.md)
 - [Tree](docs/Tree.md)
 - [User](docs/User.md)
 - [UserAddress](docs/UserAddress.md)
 - [UserBasic](docs/UserBasic.md)
 - [UserDetail](docs/UserDetail.md)
 - [UserEmail](docs/UserEmail.md)
 - [UserMessage](docs/UserMessage.md)
 - [UserNotification](docs/UserNotification.md)


## Documentation For Authorization
 Endpoints do not require authorization.


## Author



