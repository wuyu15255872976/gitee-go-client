/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

import (
	"time"
)

// 获取当前授权用户的所有Issue
type Issue struct {

	Id int32 `json:"id,omitempty"`

	Url string `json:"url,omitempty"`

	RepositoryUrl string `json:"repository_url,omitempty"`

	LabelsUrl string `json:"labels_url,omitempty"`

	EventsUrl string `json:"events_url,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	Number string `json:"number,omitempty"`

	State string `json:"state,omitempty"`

	Title string `json:"title,omitempty"`

	Body string `json:"body,omitempty"`

	User string `json:"user,omitempty"`

	Labels *Label `json:"labels,omitempty"`

	Assignee string `json:"assignee,omitempty"`

	Repository string `json:"repository,omitempty"`

	Milestone *Milestone `json:"milestone,omitempty"`

	CreatedAt time.Time `json:"created_at,omitempty"`

	UpdatedAt time.Time `json:"updated_at,omitempty"`

	Comments int32 `json:"comments,omitempty"`
}
