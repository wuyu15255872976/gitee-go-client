/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 

 */

package gitee_client

// 替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]
type Label struct {

	Id int32 `json:"id,omitempty"`

	Name string `json:"name,omitempty"`

	Color string `json:"color,omitempty"`

	RepositoryId int32 `json:"repository_id,omitempty"`

	Url string `json:"url,omitempty"`
}
