/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 

 */

package gitee_client

// 获取授权用户的地理信息
type UserAddress struct {

	Name string `json:"name,omitempty"`

	Tel string `json:"tel,omitempty"`

	Address string `json:"address,omitempty"`

	Province string `json:"province,omitempty"`

	City string `json:"city,omitempty"`

	ZipCode string `json:"zip_code,omitempty"`

	Comment string `json:"comment,omitempty"`
}
