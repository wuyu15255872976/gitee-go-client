# gitee_client\WebhooksApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoHooksId**](WebhooksApi.md#DeleteV5ReposOwnerRepoHooksId) | **Delete** /v5/repos/{owner}/{repo}/hooks/{id} | 删除一个项目WebHook
[**GetV5ReposOwnerRepoHooks**](WebhooksApi.md#GetV5ReposOwnerRepoHooks) | **Get** /v5/repos/{owner}/{repo}/hooks | 列出项目的WebHooks
[**GetV5ReposOwnerRepoHooksId**](WebhooksApi.md#GetV5ReposOwnerRepoHooksId) | **Get** /v5/repos/{owner}/{repo}/hooks/{id} | 获取项目单个WebHook
[**PatchV5ReposOwnerRepoHooksId**](WebhooksApi.md#PatchV5ReposOwnerRepoHooksId) | **Patch** /v5/repos/{owner}/{repo}/hooks/{id} | 更新一个项目WebHook
[**PostV5ReposOwnerRepoHooks**](WebhooksApi.md#PostV5ReposOwnerRepoHooks) | **Post** /v5/repos/{owner}/{repo}/hooks | 创建一个项目WebHook
[**PostV5ReposOwnerRepoHooksIdTests**](WebhooksApi.md#PostV5ReposOwnerRepoHooksIdTests) | **Post** /v5/repos/{owner}/{repo}/hooks/{id}/tests | 测试WebHook是否发送成功


# **DeleteV5ReposOwnerRepoHooksId**
> DeleteV5ReposOwnerRepoHooksId(owner, repo, id, optional)
删除一个项目WebHook

删除一个项目WebHook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| Webhook的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| Webhook的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoHooks**
> []Hook GetV5ReposOwnerRepoHooks(owner, repo, optional)
列出项目的WebHooks

列出项目的WebHooks

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoHooksId**
> Hook GetV5ReposOwnerRepoHooksId(owner, repo, id, optional)
获取项目单个WebHook

获取项目单个WebHook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| Webhook的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| Webhook的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoHooksId**
> Hook PatchV5ReposOwnerRepoHooksId(owner, repo, id, url, optional)
更新一个项目WebHook

更新一个项目WebHook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| Webhook的ID | 
  **url** | **string**| 远程HTTP URL | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| Webhook的ID | 
 **url** | **string**| 远程HTTP URL | 
 **accessToken** | **string**| 用户授权码 | 
 **password** | **string**| 请求URL时会带上该密码，防止URL被恶意请求 | 
 **pushEvents** | **bool**| Push代码到仓库 | [default to true]
 **tagPushEvents** | **bool**| 提交Tag到仓库 | 
 **issuesEvents** | **bool**| 创建/关闭Issue | 
 **noteEvents** | **bool**| 评论了Issue/代码等等 | 
 **mergeRequestsEvents** | **bool**| 合并请求和合并后 | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoHooks**
> Hook PostV5ReposOwnerRepoHooks(owner, repo, url, optional)
创建一个项目WebHook

创建一个项目WebHook

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **url** | **string**| 远程HTTP URL | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **url** | **string**| 远程HTTP URL | 
 **accessToken** | **string**| 用户授权码 | 
 **password** | **string**| 请求URL时会带上该密码，防止URL被恶意请求 | 
 **pushEvents** | **bool**| Push代码到仓库 | [default to true]
 **tagPushEvents** | **bool**| 提交Tag到仓库 | 
 **issuesEvents** | **bool**| 创建/关闭Issue | 
 **noteEvents** | **bool**| 评论了Issue/代码等等 | 
 **mergeRequestsEvents** | **bool**| 合并请求和合并后 | 

### Return type

[**Hook**](Hook.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoHooksIdTests**
> PostV5ReposOwnerRepoHooksIdTests(owner, repo, id, optional)
测试WebHook是否发送成功

测试WebHook是否发送成功

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| Webhook的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| Webhook的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

