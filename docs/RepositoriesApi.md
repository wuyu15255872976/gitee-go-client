# gitee_client\RepositoriesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepo**](RepositoriesApi.md#DeleteV5ReposOwnerRepo) | **Delete** /v5/repos/{owner}/{repo} | 删除一个项目
[**DeleteV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#DeleteV5ReposOwnerRepoBranchesBranchProtection) | **Delete** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 取消保护分支的设置
[**DeleteV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#DeleteV5ReposOwnerRepoCollaboratorsUsername) | **Delete** /v5/repos/{owner}/{repo}/collaborators/{username} | 移除项目成员
[**DeleteV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#DeleteV5ReposOwnerRepoCommentsId) | **Delete** /v5/repos/{owner}/{repo}/comments/{id} | 删除Commit评论
[**DeleteV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#DeleteV5ReposOwnerRepoContentsPath) | **Delete** /v5/repos/{owner}/{repo}/contents/{path} | 删除文件
[**DeleteV5ReposOwnerRepoKeysId**](RepositoriesApi.md#DeleteV5ReposOwnerRepoKeysId) | **Delete** /v5/repos/{owner}/{repo}/keys/{id} | 删除一个项目公钥
[**DeleteV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#DeleteV5ReposOwnerRepoReleasesId) | **Delete** /v5/repos/{owner}/{repo}/releases/{id} | 删除项目Release
[**GetV5OrgsOrgRepos**](RepositoriesApi.md#GetV5OrgsOrgRepos) | **Get** /v5/orgs/{org}/repos | 获取一个组织的项目
[**GetV5ReposOwnerRepo**](RepositoriesApi.md#GetV5ReposOwnerRepo) | **Get** /v5/repos/{owner}/{repo} | 列出授权用户的某个项目
[**GetV5ReposOwnerRepoBranches**](RepositoriesApi.md#GetV5ReposOwnerRepoBranches) | **Get** /v5/repos/{owner}/{repo}/branches | 获取所有分支
[**GetV5ReposOwnerRepoBranchesBranch**](RepositoriesApi.md#GetV5ReposOwnerRepoBranchesBranch) | **Get** /v5/repos/{owner}/{repo}/branches/{branch} | 获取单个分支
[**GetV5ReposOwnerRepoCollaborators**](RepositoriesApi.md#GetV5ReposOwnerRepoCollaborators) | **Get** /v5/repos/{owner}/{repo}/collaborators | 获取项目的所有成员
[**GetV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#GetV5ReposOwnerRepoCollaboratorsUsername) | **Get** /v5/repos/{owner}/{repo}/collaborators/{username} | 判断用户是否为项目成员
[**GetV5ReposOwnerRepoCollaboratorsUsernamePermission**](RepositoriesApi.md#GetV5ReposOwnerRepoCollaboratorsUsernamePermission) | **Get** /v5/repos/{owner}/{repo}/collaborators/{username}/permission | 查看项目成员的权限
[**GetV5ReposOwnerRepoComments**](RepositoriesApi.md#GetV5ReposOwnerRepoComments) | **Get** /v5/repos/{owner}/{repo}/comments | 获取项目的Commit评论
[**GetV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#GetV5ReposOwnerRepoCommentsId) | **Get** /v5/repos/{owner}/{repo}/comments/{id} | 获取项目的某条Commit评论
[**GetV5ReposOwnerRepoCommits**](RepositoriesApi.md#GetV5ReposOwnerRepoCommits) | **Get** /v5/repos/{owner}/{repo}/commits | 项目的所有提交
[**GetV5ReposOwnerRepoCommitsRefComments**](RepositoriesApi.md#GetV5ReposOwnerRepoCommitsRefComments) | **Get** /v5/repos/{owner}/{repo}/commits/{ref}/comments | 获取单个Commit的评论
[**GetV5ReposOwnerRepoCommitsSha**](RepositoriesApi.md#GetV5ReposOwnerRepoCommitsSha) | **Get** /v5/repos/{owner}/{repo}/commits/{sha} | 项目的某个提交
[**GetV5ReposOwnerRepoCompareBaseHead**](RepositoriesApi.md#GetV5ReposOwnerRepoCompareBaseHead) | **Get** /v5/repos/{owner}/{repo}/compare/{base}...{head} | 两个Commits之间对比的版本差异
[**GetV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#GetV5ReposOwnerRepoContentsPath) | **Get** /v5/repos/{owner}/{repo}/contents(/{path}) | 获取仓库具体路径下的内容
[**GetV5ReposOwnerRepoContributors**](RepositoriesApi.md#GetV5ReposOwnerRepoContributors) | **Get** /v5/repos/{owner}/{repo}/contributors | 获取项目贡献者
[**GetV5ReposOwnerRepoForks**](RepositoriesApi.md#GetV5ReposOwnerRepoForks) | **Get** /v5/repos/{owner}/{repo}/forks | 查看项目的Forks
[**GetV5ReposOwnerRepoKeys**](RepositoriesApi.md#GetV5ReposOwnerRepoKeys) | **Get** /v5/repos/{owner}/{repo}/keys | 展示项目的公钥
[**GetV5ReposOwnerRepoKeysId**](RepositoriesApi.md#GetV5ReposOwnerRepoKeysId) | **Get** /v5/repos/{owner}/{repo}/keys/{id} | 获取项目的单个公钥
[**GetV5ReposOwnerRepoPages**](RepositoriesApi.md#GetV5ReposOwnerRepoPages) | **Get** /v5/repos/{owner}/{repo}/pages | 获取Pages信息
[**GetV5ReposOwnerRepoReadme**](RepositoriesApi.md#GetV5ReposOwnerRepoReadme) | **Get** /v5/repos/{owner}/{repo}/readme | 获取仓库README
[**GetV5ReposOwnerRepoReleases**](RepositoriesApi.md#GetV5ReposOwnerRepoReleases) | **Get** /v5/repos/{owner}/{repo}/releases | 获取项目的所有Releases
[**GetV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#GetV5ReposOwnerRepoReleasesId) | **Get** /v5/repos/{owner}/{repo}/releases/{id} | 获取项目的单个Releases
[**GetV5ReposOwnerRepoReleasesLatest**](RepositoriesApi.md#GetV5ReposOwnerRepoReleasesLatest) | **Get** /v5/repos/{owner}/{repo}/releases/latest | 获取项目的最后更新的Release
[**GetV5ReposOwnerRepoReleasesTagsTag**](RepositoriesApi.md#GetV5ReposOwnerRepoReleasesTagsTag) | **Get** /v5/repos/{owner}/{repo}/releases/tags/{tag} | 根据Tag名称获取项目的Release
[**GetV5ReposOwnerRepoTags**](RepositoriesApi.md#GetV5ReposOwnerRepoTags) | **Get** /v5/repos/{owner}/{repo}/tags | 列出项目所有的tags
[**GetV5UserRepos**](RepositoriesApi.md#GetV5UserRepos) | **Get** /v5/user/repos | 列出授权用户的所有项目
[**GetV5UsersUsernameRepos**](RepositoriesApi.md#GetV5UsersUsernameRepos) | **Get** /v5/users/{username}/repos | 获取某个用户的公开项目
[**PatchV5ReposOwnerRepo**](RepositoriesApi.md#PatchV5ReposOwnerRepo) | **Patch** /v5/repos/{owner}/{repo} | 更新项目设置
[**PatchV5ReposOwnerRepoCommentsId**](RepositoriesApi.md#PatchV5ReposOwnerRepoCommentsId) | **Patch** /v5/repos/{owner}/{repo}/comments/{id} | 更新Commit评论
[**PatchV5ReposOwnerRepoReleasesId**](RepositoriesApi.md#PatchV5ReposOwnerRepoReleasesId) | **Patch** /v5/repos/{owner}/{repo}/releases/{id} | 更新项目Release
[**PostV5OrgsOrgRepos**](RepositoriesApi.md#PostV5OrgsOrgRepos) | **Post** /v5/orgs/{org}/repos | 创建组织项目
[**PostV5ReposOwnerRepoCommitsShaComments**](RepositoriesApi.md#PostV5ReposOwnerRepoCommitsShaComments) | **Post** /v5/repos/{owner}/{repo}/commits/{sha}/comments | 创建Commit评论
[**PostV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#PostV5ReposOwnerRepoContentsPath) | **Post** /v5/repos/{owner}/{repo}/contents/{path} | 新建文件
[**PostV5ReposOwnerRepoForks**](RepositoriesApi.md#PostV5ReposOwnerRepoForks) | **Post** /v5/repos/{owner}/{repo}/forks | Fork一个项目
[**PostV5ReposOwnerRepoKeys**](RepositoriesApi.md#PostV5ReposOwnerRepoKeys) | **Post** /v5/repos/{owner}/{repo}/keys | 为项目添加公钥
[**PostV5ReposOwnerRepoPagesBuilds**](RepositoriesApi.md#PostV5ReposOwnerRepoPagesBuilds) | **Post** /v5/repos/{owner}/{repo}/pages/builds | 请求建立Pages
[**PostV5ReposOwnerRepoReleases**](RepositoriesApi.md#PostV5ReposOwnerRepoReleases) | **Post** /v5/repos/{owner}/{repo}/releases | 创建项目Release
[**PostV5UserRepos**](RepositoriesApi.md#PostV5UserRepos) | **Post** /v5/user/repos | 创建一个项目
[**PutV5ReposOwnerRepoBranchesBranchProtection**](RepositoriesApi.md#PutV5ReposOwnerRepoBranchesBranchProtection) | **Put** /v5/repos/{owner}/{repo}/branches/{branch}/protection | 设置分支保护
[**PutV5ReposOwnerRepoCollaboratorsUsername**](RepositoriesApi.md#PutV5ReposOwnerRepoCollaboratorsUsername) | **Put** /v5/repos/{owner}/{repo}/collaborators/{username} | 添加项目成员
[**PutV5ReposOwnerRepoContentsPath**](RepositoriesApi.md#PutV5ReposOwnerRepoContentsPath) | **Put** /v5/repos/{owner}/{repo}/contents/{path} | 更新文件


# **DeleteV5ReposOwnerRepo**
> DeleteV5ReposOwnerRepo(owner, repo, optional)
删除一个项目

删除一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoBranchesBranchProtection**
> DeleteV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, optional)
取消保护分支的设置

取消保护分支的设置

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **branch** | **string**| 分支名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **branch** | **string**| 分支名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoCollaboratorsUsername**
> DeleteV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, optional)
移除项目成员

移除项目成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoCommentsId**
> DeleteV5ReposOwnerRepoCommentsId(owner, repo, id, optional)
删除Commit评论

删除Commit评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoContentsPath**
> CommitContent DeleteV5ReposOwnerRepoContentsPath(owner, repo, path, sha, message, optional)
删除文件

删除文件

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **path** | **string**| 文件的路径 | 
  **sha** | **string**| 文件被替换的sha值 | 
  **message** | **string**| 提交信息 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **path** | **string**| 文件的路径 | 
 **sha** | **string**| 文件被替换的sha值 | 
 **message** | **string**| 提交信息 | 
 **accessToken** | **string**| 用户授权码 | 
 **branch** | **string**| 分支名称。默认为项目对默认分支 | 
 **committerName** | **string**| Committer的名字，默认为当前用户的名字 | 
 **committerEmail** | **string**| Committer的邮箱，默认为当前用户的邮箱 | 
 **authorName** | **string**| Author的名字，默认为当前用户的名字 | 
 **authorEmail** | **string**| Author的邮箱，默认为当前用户的邮箱 | 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoKeysId**
> DeleteV5ReposOwnerRepoKeysId(owner, repo, id, optional)
删除一个项目公钥

删除一个项目公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 公钥 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 公钥 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoReleasesId**
> DeleteV5ReposOwnerRepoReleasesId(owner, repo, id, optional)
删除项目Release

删除项目Release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**|  | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**|  | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5OrgsOrgRepos**
> Project GetV5OrgsOrgRepos(org, optional)
获取一个组织的项目

获取一个组织的项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **org** | **string**| 组织的路径(path/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **string**| 组织的路径(path/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **type_** | **string**| 筛选项目的类型，可以是 all, public, private。默认: all | [default to all]
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepo**
> GetV5ReposOwnerRepo(owner, repo, optional)
列出授权用户的某个项目

列出授权用户的某个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoBranches**
> []Branch GetV5ReposOwnerRepoBranches(owner, repo, optional)
获取所有分支

获取所有分支

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**[]Branch**](Branch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoBranchesBranch**
> CompleteBranch GetV5ReposOwnerRepoBranchesBranch(owner, repo, branch, optional)
获取单个分支

获取单个分支

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **branch** | **string**| 分支名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **branch** | **string**| 分支名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCollaborators**
> GetV5ReposOwnerRepoCollaborators(owner, repo, optional)
获取项目的所有成员

获取项目的所有成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCollaboratorsUsername**
> GetV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, optional)
判断用户是否为项目成员

判断用户是否为项目成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCollaboratorsUsernamePermission**
> GetV5ReposOwnerRepoCollaboratorsUsernamePermission(owner, repo, username, optional)
查看项目成员的权限

查看项目成员的权限

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoComments**
> GetV5ReposOwnerRepoComments(owner, repo, optional)
获取项目的Commit评论

获取项目的Commit评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCommentsId**
> GetV5ReposOwnerRepoCommentsId(owner, repo, id, optional)
获取项目的某条Commit评论

获取项目的某条Commit评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCommits**
> []RepoCommit GetV5ReposOwnerRepoCommits(owner, repo, optional)
项目的所有提交

项目的所有提交

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **sha** | **string**| 提交起始的SHA值或者分支名. 默认: 项目的默认分支 | 
 **path** | **string**| 包含该文件的提交 | 
 **author** | **string**| 提交作者的邮箱或个性地址(username/login) | 
 **since** | **string**| 提交的起始时间，时间格式为 ISO 8601 | 
 **until** | **string**| 提交的最后时间，时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]RepoCommit**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCommitsRefComments**
> GetV5ReposOwnerRepoCommitsRefComments(owner, repo, ref, optional)
获取单个Commit的评论

获取单个Commit的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **ref** | **string**| Commit的Reference | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **ref** | **string**| Commit的Reference | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCommitsSha**
> RepoCommit GetV5ReposOwnerRepoCommitsSha(owner, repo, sha, optional)
项目的某个提交

项目的某个提交

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **sha** | **string**| 提交的SHA值或者分支名 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **sha** | **string**| 提交的SHA值或者分支名 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**RepoCommit**](RepoCommit.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoCompareBaseHead**
> Compare GetV5ReposOwnerRepoCompareBaseHead(owner, repo, base, head, optional)
两个Commits之间对比的版本差异

两个Commits之间对比的版本差异

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **base** | **string**| Commit提交的SHA值或者分支名作为对比起点 | 
  **head** | **string**| Commit提交的SHA值或者分支名作为对比终点 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **base** | **string**| Commit提交的SHA值或者分支名作为对比起点 | 
 **head** | **string**| Commit提交的SHA值或者分支名作为对比终点 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Compare**](Compare.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoContentsPath**
> []Content GetV5ReposOwnerRepoContentsPath(owner, repo, path, optional)
获取仓库具体路径下的内容

获取仓库具体路径下的内容

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **path** | **string**| 文件的路径 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **path** | **string**| 文件的路径 | 
 **accessToken** | **string**| 用户授权码 | 
 **ref** | **string**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | 

### Return type

[**[]Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoContributors**
> GetV5ReposOwnerRepoContributors(owner, repo, optional)
获取项目贡献者

获取项目贡献者

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoForks**
> GetV5ReposOwnerRepoForks(owner, repo, optional)
查看项目的Forks

查看项目的Forks

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **sort** | **string**| 排序方式: fork的时间(newest, oldest)，star的人数(stargazers) | [default to newest]
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoKeys**
> []SshKey GetV5ReposOwnerRepoKeys(owner, repo, optional)
展示项目的公钥

展示项目的公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoKeysId**
> SshKey GetV5ReposOwnerRepoKeysId(owner, repo, id, optional)
获取项目的单个公钥

获取项目的单个公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 公钥 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 公钥 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPages**
> GetV5ReposOwnerRepoPages(owner, repo, optional)
获取Pages信息

获取Pages信息

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoReadme**
> Content GetV5ReposOwnerRepoReadme(owner, repo, optional)
获取仓库README

获取仓库README

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **ref** | **string**| 分支、tag或commit。默认: 项目的默认分支(通常是master) | 

### Return type

[**Content**](Content.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoReleases**
> []Release GetV5ReposOwnerRepoReleases(owner, repo, optional)
获取项目的所有Releases

获取项目的所有Releases

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoReleasesId**
> Release GetV5ReposOwnerRepoReleasesId(owner, repo, id, optional)
获取项目的单个Releases

获取项目的单个Releases

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 发行版本的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 发行版本的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoReleasesLatest**
> Release GetV5ReposOwnerRepoReleasesLatest(owner, repo, optional)
获取项目的最后更新的Release

获取项目的最后更新的Release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoReleasesTagsTag**
> Release GetV5ReposOwnerRepoReleasesTagsTag(owner, repo, tag, optional)
根据Tag名称获取项目的Release

根据Tag名称获取项目的Release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **tag** | **string**| Tag 名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **tag** | **string**| Tag 名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoTags**
> GetV5ReposOwnerRepoTags(owner, repo, optional)
列出项目所有的tags

列出项目所有的tags

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserRepos**
> GetV5UserRepos(optional)
列出授权用户的所有项目

列出授权用户的所有项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **visibility** | **string**| 公开(public)、私有(private)或者所有(all)，默认: 所有(all) | 
 **affiliation** | **string**| owner(授权用户拥有的项目)、collaborator(授权用户为项目成员)、organization_member(授权用户为项目所在组织并有访问项目权限)。                    可以用逗号分隔符组合。如: owner, organization_member 或 owner, collaborator, organization_member | 
 **type_** | **string**| 不能与visibility或affiliation参数一并使用，否则会报422错误 | 
 **sort** | **string**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [default to full_name]
 **direction** | **string**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameRepos**
> GetV5UsersUsernameRepos(username, optional)
获取某个用户的公开项目

获取某个用户的公开项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **type_** | **string**| 用户创建的项目(owner)，用户为项目成员(member)，所有(all)。默认: 所有(all) | [default to all]
 **sort** | **string**| 排序方式: 创建时间(created)，更新时间(updated)，最后推送时间(pushed)，项目所属与名称(full_name)。默认: full_name | [default to full_name]
 **direction** | **string**| 如果sort参数为full_name，用升序(asc)。否则降序(desc) | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepo**
> PatchV5ReposOwnerRepo(owner, repo, name, optional)
更新项目设置

更新项目设置

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **name** | **string**| 项目名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **name** | **string**| 项目名称 | 
 **accessToken** | **string**| 用户授权码 | 
 **description** | **string**| 项目描述 | 
 **homepage** | **string**| 项目所在地址 | 
 **hasIssues** | **bool**| 允许提Issue与否。默认: 允许(true) | [default to true]
 **hasWiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [default to true]
 **private** | **bool**| 项目公开或私有。 | 
 **defaultBranch** | **string**| 更新默认分支 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoCommentsId**
> PatchV5ReposOwnerRepoCommentsId(owner, repo, id, body, optional)
更新Commit评论

更新Commit评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
  **body** | **string**| 评论的内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **body** | **string**| 评论的内容 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoReleasesId**
> Release PatchV5ReposOwnerRepoReleasesId(owner, repo, tagName, name, body, id, optional)
更新项目Release

更新项目Release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **tagName** | **string**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
  **name** | **string**| Release 名称 | 
  **body** | **string**| Release 描述 | 
  **id** | **int32**|  | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **tagName** | **string**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
 **name** | **string**| Release 名称 | 
 **body** | **string**| Release 描述 | 
 **id** | **int32**|  | 
 **accessToken** | **string**| 用户授权码 | 
 **prerelease** | **bool**| 是否为预览版本。默认: false（非预览版本） | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5OrgsOrgRepos**
> PostV5OrgsOrgRepos(name, org, optional)
创建组织项目

创建组织项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **name** | **string**| 项目名称 | 
  **org** | **int32**|  | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| 项目名称 | 
 **org** | **int32**|  | 
 **accessToken** | **string**| 用户授权码 | 
 **description** | **string**| 项目描述 | 
 **homepage** | **string**| 项目所在地址 | 
 **hasIssues** | **bool**| 允许提Issue与否。默认: 允许(true) | [default to true]
 **hasWiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [default to true]
 **private** | **bool**| 项目公开或私有。默认: 公开(false) | 
 **autoInit** | **bool**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | 
 **gitignoreTemplate** | **string**| Git Ingore模版 | 
 **licenseTemplate** | **string**| Git Ingore模版 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoCommitsShaComments**
> PostV5ReposOwnerRepoCommitsShaComments(owner, repo, sha, body, optional)
创建Commit评论

创建Commit评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **sha** | **string**| 评论的sha值 | 
  **body** | **string**| 评论的内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **sha** | **string**| 评论的sha值 | 
 **body** | **string**| 评论的内容 | 
 **accessToken** | **string**| 用户授权码 | 
 **path** | **string**| 文件的相对路径 | 
 **position** | **int32**| Diff的相对行数 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoContentsPath**
> CommitContent PostV5ReposOwnerRepoContentsPath(owner, repo, path, content, message, optional)
新建文件

新建文件

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **path** | **string**| 文件的路径 | 
  **content** | **string**| 文件内容, 要用base64编码 | 
  **message** | **string**| 提交信息 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **path** | **string**| 文件的路径 | 
 **content** | **string**| 文件内容, 要用base64编码 | 
 **message** | **string**| 提交信息 | 
 **accessToken** | **string**| 用户授权码 | 
 **branch** | **string**| 分支名称。默认为项目对默认分支 | 
 **committerName** | **string**| Committer的名字，默认为当前用户的名字 | 
 **committerEmail** | **string**| Committer的邮箱，默认为当前用户的邮箱 | 
 **authorName** | **string**| Author的名字，默认为当前用户的名字 | 
 **authorEmail** | **string**| Author的邮箱，默认为当前用户的邮箱 | 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoForks**
> PostV5ReposOwnerRepoForks(owner, repo, optional)
Fork一个项目

Fork一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **organization** | **string**| 组织地址，不填写默认Fork到用户个性地址 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoKeys**
> SshKey PostV5ReposOwnerRepoKeys(owner, repo, optional)
为项目添加公钥

为项目添加公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPagesBuilds**
> PostV5ReposOwnerRepoPagesBuilds(owner, repo, optional)
请求建立Pages

请求建立Pages

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoReleases**
> Release PostV5ReposOwnerRepoReleases(owner, repo, tagName, name, body, targetCommitish, optional)
创建项目Release

创建项目Release

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **tagName** | **string**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
  **name** | **string**| Release 名称 | 
  **body** | **string**| Release 描述 | 
  **targetCommitish** | **string**| 分支名称或者commit SHA, 默认是当前默认分支 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **tagName** | **string**| Tag 名称, 提倡以v字母为前缀做为Release名称，例如v1.0或者v2.3.4 | 
 **name** | **string**| Release 名称 | 
 **body** | **string**| Release 描述 | 
 **targetCommitish** | **string**| 分支名称或者commit SHA, 默认是当前默认分支 | 
 **accessToken** | **string**| 用户授权码 | 
 **prerelease** | **bool**| 是否为预览版本。默认: false（非预览版本） | 

### Return type

[**Release**](Release.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5UserRepos**
> PostV5UserRepos(name, optional)
创建一个项目

创建一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **name** | **string**| 项目名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **string**| 项目名称 | 
 **accessToken** | **string**| 用户授权码 | 
 **description** | **string**| 项目描述 | 
 **homepage** | **string**| 项目所在地址 | 
 **hasIssues** | **bool**| 允许提Issue与否。默认: 允许(true) | [default to true]
 **hasWiki** | **bool**| 提供Wiki与否。默认: 提供(true) | [default to true]
 **private** | **bool**| 项目公开或私有。默认: 公开(false) | 
 **autoInit** | **bool**| 值为true时则会用README初始化仓库。默认: 不初始化(false) | 
 **gitignoreTemplate** | **string**| Git Ingore模版 | 
 **licenseTemplate** | **string**| License模版 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoBranchesBranchProtection**
> CompleteBranch PutV5ReposOwnerRepoBranchesBranchProtection(owner, repo, branch, optional)
设置分支保护

设置分支保护

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **branch** | **string**| 分支名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **branch** | **string**| 分支名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CompleteBranch**](CompleteBranch.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoCollaboratorsUsername**
> PutV5ReposOwnerRepoCollaboratorsUsername(owner, repo, username, permission, optional)
添加项目成员

添加项目成员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **username** | **string**| 用户名(username/login) | 
  **permission** | **string**| 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push | [default to push]
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **username** | **string**| 用户名(username/login) | 
 **permission** | **string**| 成员权限: 拉代码(pull)，推代码(push)，管理员(admin)。默认: push | [default to push]
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoContentsPath**
> CommitContent PutV5ReposOwnerRepoContentsPath(owner, repo, path, content, sha, message, optional)
更新文件

更新文件

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **path** | **string**| 文件的路径 | 
  **content** | **string**| 文件内容, 要用base64编码 | 
  **sha** | **string**| 文件被替换的sha值 | 
  **message** | **string**| 提交信息 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **path** | **string**| 文件的路径 | 
 **content** | **string**| 文件内容, 要用base64编码 | 
 **sha** | **string**| 文件被替换的sha值 | 
 **message** | **string**| 提交信息 | 
 **accessToken** | **string**| 用户授权码 | 
 **branch** | **string**| 分支名称。默认为项目对默认分支 | 
 **committerName** | **string**| Committer的名字，默认为当前用户的名字 | 
 **committerEmail** | **string**| Committer的邮箱，默认为当前用户的邮箱 | 
 **authorName** | **string**| Author的名字，默认为当前用户的名字 | 
 **authorEmail** | **string**| Author的邮箱，默认为当前用户的邮箱 | 

### Return type

[**CommitContent**](CommitContent.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

