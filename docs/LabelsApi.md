# gitee_client\LabelsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#DeleteV5ReposOwnerRepoIssuesNumberLabels) | **Delete** /v5/repos/{owner}/{repo}/issues/{number}/labels | 删除Issue所有标签
[**DeleteV5ReposOwnerRepoIssuesNumberLabelsName**](LabelsApi.md#DeleteV5ReposOwnerRepoIssuesNumberLabelsName) | **Delete** /v5/repos/{owner}/{repo}/issues/{number}/labels/{name} | 删除Issue标签
[**DeleteV5ReposOwnerRepoLabelsName**](LabelsApi.md#DeleteV5ReposOwnerRepoLabelsName) | **Delete** /v5/repos/{owner}/{repo}/labels/{name} | 删除一个项目标签
[**GetV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#GetV5ReposOwnerRepoIssuesNumberLabels) | **Get** /v5/repos/{owner}/{repo}/issues/{number}/labels | 获取项目Issue的所有标签
[**GetV5ReposOwnerRepoLabels**](LabelsApi.md#GetV5ReposOwnerRepoLabels) | **Get** /v5/repos/{owner}/{repo}/labels | 获取项目所有标签
[**GetV5ReposOwnerRepoLabelsName**](LabelsApi.md#GetV5ReposOwnerRepoLabelsName) | **Get** /v5/repos/{owner}/{repo}/labels/{name} | 根据标签名称获取单个标签
[**PatchV5ReposOwnerRepoLabelsOriginalName**](LabelsApi.md#PatchV5ReposOwnerRepoLabelsOriginalName) | **Patch** /v5/repos/{owner}/{repo}/labels/{original_name} | 更新一个项目标签
[**PostV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#PostV5ReposOwnerRepoIssuesNumberLabels) | **Post** /v5/repos/{owner}/{repo}/issues/{number}/labels | 创建Issue标签
[**PostV5ReposOwnerRepoLabels**](LabelsApi.md#PostV5ReposOwnerRepoLabels) | **Post** /v5/repos/{owner}/{repo}/labels | 创建项目标签
[**PutV5ReposOwnerRepoIssuesNumberLabels**](LabelsApi.md#PutV5ReposOwnerRepoIssuesNumberLabels) | **Put** /v5/repos/{owner}/{repo}/issues/{number}/labels | 替换Issue所有标签


# **DeleteV5ReposOwnerRepoIssuesNumberLabels**
> DeleteV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, optional)
删除Issue所有标签

删除Issue所有标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoIssuesNumberLabelsName**
> DeleteV5ReposOwnerRepoIssuesNumberLabelsName(owner, repo, number, name, optional)
删除Issue标签

删除Issue标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
  **name** | **string**| 标签名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **name** | **string**| 标签名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoLabelsName**
> DeleteV5ReposOwnerRepoLabelsName(owner, repo, name, optional)
删除一个项目标签

删除一个项目标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **name** | **string**| 标签名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **name** | **string**| 标签名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssuesNumberLabels**
> []Label GetV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, optional)
获取项目Issue的所有标签

获取项目Issue的所有标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**[]Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoLabels**
> []Label GetV5ReposOwnerRepoLabels(owner, repo, optional)
获取项目所有标签

获取项目所有标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**[]Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoLabelsName**
> Label GetV5ReposOwnerRepoLabelsName(owner, repo, name, optional)
根据标签名称获取单个标签

根据标签名称获取单个标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **name** | **string**| 标签名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **name** | **string**| 标签名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoLabelsOriginalName**
> Label PatchV5ReposOwnerRepoLabelsOriginalName(owner, repo, originalName, optional)
更新一个项目标签

更新一个项目标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **originalName** | **int32**|  | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **originalName** | **int32**|  | 
 **accessToken** | **string**| 用户授权码 | 
 **name** | **string**| The name of a label | 
 **color** | **string**| The color of a label | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoIssuesNumberLabels**
> Label PostV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, optional)
创建Issue标签

创建Issue标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoLabels**
> Label PostV5ReposOwnerRepoLabels(owner, repo, name, color, optional)
创建项目标签

创建项目标签

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **name** | **string**| 标签名称 | 
  **color** | **string**| 标签颜色。为6位的数字，如: 000000 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **name** | **string**| 标签名称 | 
 **color** | **string**| 标签颜色。为6位的数字，如: 000000 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoIssuesNumberLabels**
> Label PutV5ReposOwnerRepoIssuesNumberLabels(owner, repo, number, optional)
替换Issue所有标签

替换Issue所有标签  需要在请求的body里填上数组，元素为标签的名字。如: [\"performance\", \"bug\"]

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Label**](Label.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

