# gitee_client\ActivityApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5UserStarredOwnerRepo**](ActivityApi.md#DeleteV5UserStarredOwnerRepo) | **Delete** /v5/user/starred/{owner}/{repo} | 取消 star 一个项目
[**DeleteV5UserSubscriptionsOwnerRepo**](ActivityApi.md#DeleteV5UserSubscriptionsOwnerRepo) | **Delete** /v5/user/subscriptions/{owner}/{repo} | 取消 watch 一个项目
[**GetV5Events**](ActivityApi.md#GetV5Events) | **Get** /v5/events | 获取站内所有公开动态
[**GetV5NetworksOwnerRepoEvents**](ActivityApi.md#GetV5NetworksOwnerRepoEvents) | **Get** /v5/networks/{owner}/{repo}/events | 列出项目的所有公开动态
[**GetV5NotificationsMessages**](ActivityApi.md#GetV5NotificationsMessages) | **Get** /v5/notifications/messages | 列出授权用户的所有私信
[**GetV5NotificationsMessagesId**](ActivityApi.md#GetV5NotificationsMessagesId) | **Get** /v5/notifications/messages/{id} | 获取一个私信
[**GetV5NotificationsThreads**](ActivityApi.md#GetV5NotificationsThreads) | **Get** /v5/notifications/threads | 列出授权用户的所有通知
[**GetV5NotificationsThreadsId**](ActivityApi.md#GetV5NotificationsThreadsId) | **Get** /v5/notifications/threads/{id} | 获取一个通知
[**GetV5OrgsOrgEvents**](ActivityApi.md#GetV5OrgsOrgEvents) | **Get** /v5/orgs/{org}/events | 列出组织的公开动态
[**GetV5ReposOwnerRepoEvents**](ActivityApi.md#GetV5ReposOwnerRepoEvents) | **Get** /v5/repos/{owner}/{repo}/events | 列出项目的所有动态
[**GetV5ReposOwnerRepoNotifications**](ActivityApi.md#GetV5ReposOwnerRepoNotifications) | **Get** /v5/repos/{owner}/{repo}/notifications | 列出一个项目里的通知
[**GetV5ReposOwnerRepoStargazers**](ActivityApi.md#GetV5ReposOwnerRepoStargazers) | **Get** /v5/repos/{owner}/{repo}/stargazers | 列出 star 了项目的用户
[**GetV5ReposOwnerRepoSubscribers**](ActivityApi.md#GetV5ReposOwnerRepoSubscribers) | **Get** /v5/repos/{owner}/{repo}/subscribers | 列出 watch 了项目的用户
[**GetV5UserStarred**](ActivityApi.md#GetV5UserStarred) | **Get** /v5/user/starred | 列出授权用户 star 了的项目
[**GetV5UserStarredOwnerRepo**](ActivityApi.md#GetV5UserStarredOwnerRepo) | **Get** /v5/user/starred/{owner}/{repo} | 检查授权用户是否 star 了一个项目
[**GetV5UserSubscriptions**](ActivityApi.md#GetV5UserSubscriptions) | **Get** /v5/user/subscriptions | 列出授权用户 watch 了的项目
[**GetV5UserSubscriptionsOwnerRepo**](ActivityApi.md#GetV5UserSubscriptionsOwnerRepo) | **Get** /v5/user/subscriptions/{owner}/{repo} | 检查授权用户是否 watch 了一个项目
[**GetV5UsersUsernameEvents**](ActivityApi.md#GetV5UsersUsernameEvents) | **Get** /v5/users/{username}/events | 列出用户的动态
[**GetV5UsersUsernameEventsOrgsOrg**](ActivityApi.md#GetV5UsersUsernameEventsOrgsOrg) | **Get** /v5/users/{username}/events/orgs/{org} | 列出用户所属组织的动态
[**GetV5UsersUsernameEventsPublic**](ActivityApi.md#GetV5UsersUsernameEventsPublic) | **Get** /v5/users/{username}/events/public | 列出用户的公开动态
[**GetV5UsersUsernameReceivedEvents**](ActivityApi.md#GetV5UsersUsernameReceivedEvents) | **Get** /v5/users/{username}/received_events | 列出一个用户收到的动态
[**GetV5UsersUsernameReceivedEventsPublic**](ActivityApi.md#GetV5UsersUsernameReceivedEventsPublic) | **Get** /v5/users/{username}/received_events/public | 列出一个用户收到的公开动态
[**GetV5UsersUsernameStarred**](ActivityApi.md#GetV5UsersUsernameStarred) | **Get** /v5/users/{username}/starred | 列出用户 star 了的项目
[**GetV5UsersUsernameSubscriptions**](ActivityApi.md#GetV5UsersUsernameSubscriptions) | **Get** /v5/users/{username}/subscriptions | 列出用户 watch 了的项目
[**PatchV5NotificationsMessagesId**](ActivityApi.md#PatchV5NotificationsMessagesId) | **Patch** /v5/notifications/messages/{id} | 标记一个私信为已读
[**PatchV5NotificationsThreadsId**](ActivityApi.md#PatchV5NotificationsThreadsId) | **Patch** /v5/notifications/threads/{id} | 标记一个通知为已读
[**PostV5NotificationsMessages**](ActivityApi.md#PostV5NotificationsMessages) | **Post** /v5/notifications/messages | 发送私信给指定用户
[**PutV5NotificationsMessages**](ActivityApi.md#PutV5NotificationsMessages) | **Put** /v5/notifications/messages | 标记所有私信为已读
[**PutV5NotificationsThreads**](ActivityApi.md#PutV5NotificationsThreads) | **Put** /v5/notifications/threads | 标记所有通知为已读
[**PutV5ReposOwnerRepoNotifications**](ActivityApi.md#PutV5ReposOwnerRepoNotifications) | **Put** /v5/repos/{owner}/{repo}/notifications | 标记一个项目里的通知为已读
[**PutV5UserStarredOwnerRepo**](ActivityApi.md#PutV5UserStarredOwnerRepo) | **Put** /v5/user/starred/{owner}/{repo} | star 一个项目
[**PutV5UserSubscriptionsOwnerRepo**](ActivityApi.md#PutV5UserSubscriptionsOwnerRepo) | **Put** /v5/user/subscriptions/{owner}/{repo} | watch 一个项目


# **DeleteV5UserStarredOwnerRepo**
> DeleteV5UserStarredOwnerRepo(owner, repo, optional)
取消 star 一个项目

取消 star 一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5UserSubscriptionsOwnerRepo**
> DeleteV5UserSubscriptionsOwnerRepo(owner, repo, optional)
取消 watch 一个项目

取消 watch 一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5Events**
> []Event GetV5Events(optional)
获取站内所有公开动态

获取站内所有公开动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5NetworksOwnerRepoEvents**
> []Event GetV5NetworksOwnerRepoEvents(owner, repo, optional)
列出项目的所有公开动态

列出项目的所有公开动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5NotificationsMessages**
> []UserMessage GetV5NotificationsMessages(optional)
列出授权用户的所有私信

列出授权用户的所有私信

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **unread** | **bool**| 是否只显示未读私信，默认：否 | 
 **since** | **string**| 只显示在给定时间后更新的私信，要求时间格式为 ISO 8601 | 
 **before** | **string**| 只显示在给定时间前更新的私信，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserMessage**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5NotificationsMessagesId**
> UserMessage GetV5NotificationsMessagesId(id, optional)
获取一个私信

获取一个私信

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 私信的 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 私信的 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserMessage**](UserMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5NotificationsThreads**
> []UserNotification GetV5NotificationsThreads(optional)
列出授权用户的所有通知

列出授权用户的所有通知

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **unread** | **bool**| 是否只显示未读消息，默认：否 | 
 **participating** | **bool**| 是否只显示自己直接参与的消息，默认：否 | 
 **since** | **string**| 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 | 
 **before** | **string**| 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserNotification**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5NotificationsThreadsId**
> UserNotification GetV5NotificationsThreadsId(id, optional)
获取一个通知

获取一个通知

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 通知的 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 通知的 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserNotification**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5OrgsOrgEvents**
> []Event GetV5OrgsOrgEvents(org, optional)
列出组织的公开动态

列出组织的公开动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **org** | **string**| 组织的路径(path/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **string**| 组织的路径(path/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoEvents**
> []Event GetV5ReposOwnerRepoEvents(owner, repo, optional)
列出项目的所有动态

列出项目的所有动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoNotifications**
> []UserNotification GetV5ReposOwnerRepoNotifications(owner, repo, optional)
列出一个项目里的通知

列出一个项目里的通知

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **unread** | **bool**| 是否只显示未读消息，默认：否 | 
 **participating** | **bool**| 是否只显示自己直接参与的消息，默认：否 | 
 **since** | **string**| 只显示在给定时间后更新的消息，要求时间格式为 ISO 8601 | 
 **before** | **string**| 只显示在给定时间前更新的消息，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserNotification**](UserNotification.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoStargazers**
> []UserBasic GetV5ReposOwnerRepoStargazers(owner, repo, optional)
列出 star 了项目的用户

列出 star 了项目的用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoSubscribers**
> []UserBasic GetV5ReposOwnerRepoSubscribers(owner, repo, optional)
列出 watch 了项目的用户

列出 watch 了项目的用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserStarred**
> []Project GetV5UserStarred(optional)
列出授权用户 star 了的项目

列出授权用户 star 了的项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **sort** | **string**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [default to created]
 **direction** | **string**| 按递增(asc)或递减(desc)排序，默认：递减 | [default to desc]
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserStarredOwnerRepo**
> GetV5UserStarredOwnerRepo(owner, repo, optional)
检查授权用户是否 star 了一个项目

检查授权用户是否 star 了一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserSubscriptions**
> []Project GetV5UserSubscriptions(optional)
列出授权用户 watch 了的项目

列出授权用户 watch 了的项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **sort** | **string**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [default to created]
 **direction** | **string**| 按递增(asc)或递减(desc)排序，默认：递减 | [default to desc]
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserSubscriptionsOwnerRepo**
> GetV5UserSubscriptionsOwnerRepo(owner, repo, optional)
检查授权用户是否 watch 了一个项目

检查授权用户是否 watch 了一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameEvents**
> []Event GetV5UsersUsernameEvents(username, optional)
列出用户的动态

列出用户的动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameEventsOrgsOrg**
> []Event GetV5UsersUsernameEventsOrgsOrg(username, org, optional)
列出用户所属组织的动态

列出用户所属组织的动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
  **org** | **string**| 组织的路径(path/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **org** | **string**| 组织的路径(path/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameEventsPublic**
> []Event GetV5UsersUsernameEventsPublic(username, optional)
列出用户的公开动态

列出用户的公开动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameReceivedEvents**
> []Event GetV5UsersUsernameReceivedEvents(username, optional)
列出一个用户收到的动态

列出一个用户收到的动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameReceivedEventsPublic**
> []Event GetV5UsersUsernameReceivedEventsPublic(username, optional)
列出一个用户收到的公开动态

列出一个用户收到的公开动态

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Event**](Event.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameStarred**
> []Project GetV5UsersUsernameStarred(username, optional)
列出用户 star 了的项目

列出用户 star 了的项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]
 **sort** | **string**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [default to created]
 **direction** | **string**| 按递增(asc)或递减(desc)排序，默认：递减 | [default to desc]

### Return type

[**[]Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameSubscriptions**
> []Project GetV5UsersUsernameSubscriptions(username, optional)
列出用户 watch 了的项目

列出用户 watch 了的项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]
 **sort** | **string**| 根据项目创建时间(created)或最后推送时间(updated)进行排序，默认：创建时间 | [default to created]
 **direction** | **string**| 按递增(asc)或递减(desc)排序，默认：递减 | [default to desc]

### Return type

[**[]Project**](Project.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5NotificationsMessagesId**
> PatchV5NotificationsMessagesId(id, optional)
标记一个私信为已读

标记一个私信为已读

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 私信的 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 私信的 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5NotificationsThreadsId**
> PatchV5NotificationsThreadsId(id, optional)
标记一个通知为已读

标记一个通知为已读

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 通知的 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 通知的 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5NotificationsMessages**
> PostV5NotificationsMessages(username, content, optional)
发送私信给指定用户

发送私信给指定用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
  **content** | **string**| 私信内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **content** | **string**| 私信内容 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5NotificationsMessages**
> PutV5NotificationsMessages(optional)
标记所有私信为已读

标记所有私信为已读

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5NotificationsThreads**
> PutV5NotificationsThreads(optional)
标记所有通知为已读

标记所有通知为已读

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoNotifications**
> PutV5ReposOwnerRepoNotifications(owner, repo, optional)
标记一个项目里的通知为已读

标记一个项目里的通知为已读

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5UserStarredOwnerRepo**
> PutV5UserStarredOwnerRepo(owner, repo, optional)
star 一个项目

star 一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5UserSubscriptionsOwnerRepo**
> PutV5UserSubscriptionsOwnerRepo(owner, repo, optional)
watch 一个项目

watch 一个项目

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

