# gitee_client\PullRequestsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsCommentsId) | **Delete** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 删除评论
[**DeleteV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#DeleteV5ReposOwnerRepoPullsNumberRequestedReviewers) | **Delete** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 移除审查人员
[**GetV5ReposOwnerRepoPulls**](PullRequestsApi.md#GetV5ReposOwnerRepoPulls) | **Get** /v5/repos/{owner}/{repo}/pulls | 获取Pull Request列表
[**GetV5ReposOwnerRepoPullsComments**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsComments) | **Get** /v5/repos/{owner}/{repo}/pulls/comments | 获取该项目下的所有Pull Request评论
[**GetV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsCommentsId) | **Get** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 获取Pull Request的某个评论
[**GetV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumber) | **Get** /v5/repos/{owner}/{repo}/pulls/{number} | 获取单个Pull Request
[**GetV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberComments) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 获取某个Pull Request的所有评论
[**GetV5ReposOwnerRepoPullsNumberCommits**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberCommits) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/commits | 获取某Pull Request的所有Commit信息。最多显示250条Commit
[**GetV5ReposOwnerRepoPullsNumberFiles**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberFiles) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/files | Pull Request Commit文件列表。最多显示300条diff
[**GetV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberMerge) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 判断Pull Request是否已经合并
[**GetV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#GetV5ReposOwnerRepoPullsNumberRequestedReviewers) | **Get** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 获取审查人员的列表
[**PatchV5ReposOwnerRepoPullsCommentsId**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsCommentsId) | **Patch** /v5/repos/{owner}/{repo}/pulls/comments/{id} | 编辑评论
[**PatchV5ReposOwnerRepoPullsNumber**](PullRequestsApi.md#PatchV5ReposOwnerRepoPullsNumber) | **Patch** /v5/repos/{owner}/{repo}/pulls/{number} | 更新Pull Request信息
[**PostV5ReposOwnerRepoPulls**](PullRequestsApi.md#PostV5ReposOwnerRepoPulls) | **Post** /v5/repos/{owner}/{repo}/pulls | 创建Pull Request
[**PostV5ReposOwnerRepoPullsNumberComments**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberComments) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/comments | 提交Pull Request评论
[**PostV5ReposOwnerRepoPullsNumberRequestedReviewers**](PullRequestsApi.md#PostV5ReposOwnerRepoPullsNumberRequestedReviewers) | **Post** /v5/repos/{owner}/{repo}/pulls/{number}/requested_reviewers | 增加审查人员
[**PutV5ReposOwnerRepoPullsNumberMerge**](PullRequestsApi.md#PutV5ReposOwnerRepoPullsNumberMerge) | **Put** /v5/repos/{owner}/{repo}/pulls/{number}/merge | 合并Pull Request


# **DeleteV5ReposOwnerRepoPullsCommentsId**
> DeleteV5ReposOwnerRepoPullsCommentsId(owner, repo, id, optional)
删除评论

删除评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5ReposOwnerRepoPullsNumberRequestedReviewers**
> DeleteV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, optional)
移除审查人员

移除审查人员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
  **reviewers** | [**[]string**](string.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对username换行即可 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **reviewers** | [**[]string**](string.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对username换行即可 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPulls**
> []PullRequest GetV5ReposOwnerRepoPulls(owner, repo, optional)
获取Pull Request列表

获取Pull Request列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| 可选。Pull Request 状态 | [default to open]
 **head** | **string**| 可选。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
 **base** | **string**| 可选。Pull Request 提交目标分支的名称。 | 
 **sort** | **string**| 可选。排序字段，默认按创建时间 | [default to created]
 **direction** | **string**| 可选。升序/降序 | [default to desc]
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsComments**
> []PullRequestComments GetV5ReposOwnerRepoPullsComments(owner, repo, optional)
获取该项目下的所有Pull Request评论

获取该项目下的所有Pull Request评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **sort** | **string**| 可选。按创建时间/更新时间排序 | [default to created]
 **direction** | **string**| 可选。升序/降序 | [default to desc]
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments GetV5ReposOwnerRepoPullsCommentsId(owner, repo, id, optional)
获取Pull Request的某个评论

获取Pull Request的某个评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**|  | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**|  | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumber**
> PullRequest GetV5ReposOwnerRepoPullsNumber(owner, repo, number, optional)
获取单个Pull Request

获取单个Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberComments**
> []PullRequestComments GetV5ReposOwnerRepoPullsNumberComments(owner, repo, number, optional)
获取某个Pull Request的所有评论

获取某个Pull Request的所有评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberCommits**
> []PullRequestCommits GetV5ReposOwnerRepoPullsNumberCommits(owner, repo, number, optional)
获取某Pull Request的所有Commit信息。最多显示250条Commit

获取某Pull Request的所有Commit信息。最多显示250条Commit

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**[]PullRequestCommits**](PullRequestCommits.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberFiles**
> []PullRequestFiles GetV5ReposOwnerRepoPullsNumberFiles(owner, repo, number, optional)
Pull Request Commit文件列表。最多显示300条diff

Pull Request Commit文件列表。最多显示300条diff

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**[]PullRequestFiles**](PullRequestFiles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberMerge**
> GetV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, optional)
判断Pull Request是否已经合并

判断Pull Request是否已经合并

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoPullsNumberRequestedReviewers**
> UserBasic GetV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, optional)
获取审查人员的列表

获取审查人员的列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsCommentsId**
> PullRequestComments PatchV5ReposOwnerRepoPullsCommentsId(owner, repo, id, body, optional)
编辑评论

编辑评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
  **body** | **string**| 必填。评论内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **body** | **string**| 必填。评论内容 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoPullsNumber**
> PullRequest PatchV5ReposOwnerRepoPullsNumber(owner, repo, number, optional)
更新Pull Request信息

更新Pull Request信息

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 
 **title** | **string**| 可选。Pull Request 标题 | 
 **body** | **string**| 可选。Pull Request 内容 | 
 **state** | **string**| 可选。Pull Request 状态 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPulls**
> PullRequest PostV5ReposOwnerRepoPulls(owner, repo, title, head, base, optional)
创建Pull Request

创建Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **title** | **string**| 必填。Pull Request 标题 | 
  **head** | **string**| 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
  **base** | **string**| 必填。Pull Request 提交目标分支的名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **title** | **string**| 必填。Pull Request 标题 | 
 **head** | **string**| 必填。Pull Request 提交的源分支。格式：branch 或者：username:branch | 
 **base** | **string**| 必填。Pull Request 提交目标分支的名称 | 
 **accessToken** | **string**| 用户授权码 | 
 **body** | **string**| 可选。Pull Request 内容 | 
 **issue** | **string**| 可选。Pull Request的标题和内容可以根据指定的Issue Id自动填充 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberComments**
> PullRequestComments PostV5ReposOwnerRepoPullsNumberComments(owner, repo, number, body, optional)
提交Pull Request评论

提交Pull Request评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
  **body** | **string**| 必填。评论内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **body** | **string**| 必填。评论内容 | 
 **accessToken** | **string**| 用户授权码 | 
 **commitId** | **string**| 可选。PR代码评论的commit id | 
 **path** | **string**| 可选。PR代码评论的文件名 | 
 **position** | **int32**| 可选。PR代码评论diff中的行数 | 

### Return type

[**PullRequestComments**](PullRequestComments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoPullsNumberRequestedReviewers**
> PullRequest PostV5ReposOwnerRepoPullsNumberRequestedReviewers(owner, repo, number, reviewers, optional)
增加审查人员

增加审查人员

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
  **reviewers** | [**[]string**](string.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对标签名换行即可 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **reviewers** | [**[]string**](string.md)| 审核人员的username，数组形式: [\&quot;oschina\&quot;, \&quot;tom\&quot;], 此处试验直接对标签名换行即可 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**PullRequest**](PullRequest.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5ReposOwnerRepoPullsNumberMerge**
> PutV5ReposOwnerRepoPullsNumberMerge(owner, repo, number, optional)
合并Pull Request

合并Pull Request

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个PR，即本项目PR的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

