# gitee_client\UsersApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5UserFollowingUsername**](UsersApi.md#DeleteV5UserFollowingUsername) | **Delete** /v5/user/following/{username} | 取消关注一个用户
[**DeleteV5UserKeysId**](UsersApi.md#DeleteV5UserKeysId) | **Delete** /v5/user/keys/{id} | 删除一个公钥
[**DeleteV5UserUnconfirmedEmail**](UsersApi.md#DeleteV5UserUnconfirmedEmail) | **Delete** /v5/user/unconfirmed_email | 删除授权用户未激活的邮箱地址
[**GetV5User**](UsersApi.md#GetV5User) | **Get** /v5/user | 获取授权用户的资料
[**GetV5UserAddress**](UsersApi.md#GetV5UserAddress) | **Get** /v5/user/address | 获取授权用户的地理信息
[**GetV5UserEmails**](UsersApi.md#GetV5UserEmails) | **Get** /v5/user/emails | 获取授权用户的邮箱地址
[**GetV5UserFollowers**](UsersApi.md#GetV5UserFollowers) | **Get** /v5/user/followers | 列出授权用户的关注者
[**GetV5UserFollowing**](UsersApi.md#GetV5UserFollowing) | **Get** /v5/user/following | 列出授权用户正关注的用户
[**GetV5UserFollowingUsername**](UsersApi.md#GetV5UserFollowingUsername) | **Get** /v5/user/following/{username} | 检查授权用户是否关注了一个用户
[**GetV5UserKeys**](UsersApi.md#GetV5UserKeys) | **Get** /v5/user/keys | 列出授权用户的所有公钥
[**GetV5UserKeysId**](UsersApi.md#GetV5UserKeysId) | **Get** /v5/user/keys/{id} | 获取一个公钥
[**GetV5UsersUsername**](UsersApi.md#GetV5UsersUsername) | **Get** /v5/users/{username} | 获取一个用户
[**GetV5UsersUsernameFollowers**](UsersApi.md#GetV5UsersUsernameFollowers) | **Get** /v5/users/{username}/followers | 列出指定用户的关注者
[**GetV5UsersUsernameFollowing**](UsersApi.md#GetV5UsersUsernameFollowing) | **Get** /v5/users/{username}/following | 列出指定用户正在关注的用户
[**GetV5UsersUsernameFollowingTargetUser**](UsersApi.md#GetV5UsersUsernameFollowingTargetUser) | **Get** /v5/users/{username}/following/{target_user} | 检查指定用户是否关注目标用户
[**GetV5UsersUsernameKeys**](UsersApi.md#GetV5UsersUsernameKeys) | **Get** /v5/users/{username}/keys | 列出指定用户的所有公钥
[**PatchV5User**](UsersApi.md#PatchV5User) | **Patch** /v5/user | 更新授权用户的资料
[**PatchV5UserAddress**](UsersApi.md#PatchV5UserAddress) | **Patch** /v5/user/address | 更新授权用户的地理信息
[**PostV5UserEmails**](UsersApi.md#PostV5UserEmails) | **Post** /v5/user/emails | 添加授权用户的新邮箱地址
[**PostV5UserKeys**](UsersApi.md#PostV5UserKeys) | **Post** /v5/user/keys | 添加一个公钥
[**PutV5UserFollowingUsername**](UsersApi.md#PutV5UserFollowingUsername) | **Put** /v5/user/following/{username} | 关注一个用户


# **DeleteV5UserFollowingUsername**
> DeleteV5UserFollowingUsername(username, optional)
取消关注一个用户

取消关注一个用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5UserKeysId**
> DeleteV5UserKeysId(id, optional)
删除一个公钥

删除一个公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 公钥 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 公钥 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5UserUnconfirmedEmail**
> DeleteV5UserUnconfirmedEmail(optional)
删除授权用户未激活的邮箱地址

删除授权用户未激活的邮箱地址

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5User**
> UserDetail GetV5User(optional)
获取授权用户的资料

获取授权用户的资料

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserAddress**
> UserAddress GetV5UserAddress(optional)
获取授权用户的地理信息

获取授权用户的地理信息

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserAddress**](UserAddress.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserEmails**
> UserEmail GetV5UserEmails(optional)
获取授权用户的邮箱地址

获取授权用户的邮箱地址

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserEmail**](UserEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserFollowers**
> []UserBasic GetV5UserFollowers(optional)
列出授权用户的关注者

列出授权用户的关注者

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserFollowing**
> []UserBasic GetV5UserFollowing(optional)
列出授权用户正关注的用户

列出授权用户正关注的用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserFollowingUsername**
> GetV5UserFollowingUsername(username, optional)
检查授权用户是否关注了一个用户

检查授权用户是否关注了一个用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserKeys**
> []SshKey GetV5UserKeys(optional)
列出授权用户的所有公钥

列出授权用户的所有公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserKeysId**
> SshKey GetV5UserKeysId(id, optional)
获取一个公钥

获取一个公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **int32**| 公钥 ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int32**| 公钥 ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsername**
> User GetV5UsersUsername(username, optional)
获取一个用户

获取一个用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameFollowers**
> []UserBasic GetV5UsersUsernameFollowers(username, optional)
列出指定用户的关注者

列出指定用户的关注者

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameFollowing**
> []UserBasic GetV5UsersUsernameFollowing(username, optional)
列出指定用户正在关注的用户

列出指定用户正在关注的用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]UserBasic**](UserBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameFollowingTargetUser**
> GetV5UsersUsernameFollowingTargetUser(username, targetUser, optional)
检查指定用户是否关注目标用户

检查指定用户是否关注目标用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
  **targetUser** | **string**| 目标用户的用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **targetUser** | **string**| 目标用户的用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameKeys**
> []SshKeyBasic GetV5UsersUsernameKeys(username, optional)
列出指定用户的所有公钥

列出指定用户的所有公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]SshKeyBasic**](SSHKeyBasic.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5User**
> UserDetail PatchV5User(optional)
更新授权用户的资料

更新授权用户的资料

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **name** | **string**| 昵称 | 
 **blog** | **string**| 微博链接 | 
 **weibo** | **string**| 博客站点 | 
 **bio** | **string**| 自我介绍 | 

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5UserAddress**
> UserDetail PatchV5UserAddress(optional)
更新授权用户的地理信息

更新授权用户的地理信息

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **name** | **string**| 联系人名 | 
 **tel** | **string**| 联系电话 | 
 **address** | **string**| 联系地址 | 
 **province** | **string**| 省份 | 
 **city** | **string**| 城市 | 
 **zipCode** | **string**| 邮政编码 | 
 **comment** | **string**| 备注 | 

### Return type

[**UserDetail**](UserDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5UserEmails**
> UserEmail PostV5UserEmails(email, optional)
添加授权用户的新邮箱地址

添加授权用户的新邮箱地址

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **email** | **string**| 新的邮箱地址 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **string**| 新的邮箱地址 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**UserEmail**](UserEmail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5UserKeys**
> SshKey PostV5UserKeys(key, title, optional)
添加一个公钥

添加一个公钥

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **key** | **string**| 公钥内容. | 
  **title** | **string**| 公钥名称 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **string**| 公钥内容. | 
 **title** | **string**| 公钥名称 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**SshKey**](SSHKey.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5UserFollowingUsername**
> PutV5UserFollowingUsername(username, optional)
关注一个用户

关注一个用户

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

