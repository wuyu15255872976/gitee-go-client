# gitee_client\GitDataApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetV5ReposOwnerRepoGitBlobsSha**](GitDataApi.md#GetV5ReposOwnerRepoGitBlobsSha) | **Get** /v5/repos/{owner}/{repo}/git/blobs/{sha} | 获取文件Blob
[**GetV5ReposOwnerRepoGitTreesSha**](GitDataApi.md#GetV5ReposOwnerRepoGitTreesSha) | **Get** /v5/repos/{owner}/{repo}/git/trees/{sha} | 获取目录Tree


# **GetV5ReposOwnerRepoGitBlobsSha**
> Blob GetV5ReposOwnerRepoGitBlobsSha(owner, repo, sha, optional)
获取文件Blob

获取文件Blob

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **sha** | **string**| 文件Blob的SHA值 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **sha** | **string**| 文件Blob的SHA值 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Blob**](Blob.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoGitTreesSha**
> Tree GetV5ReposOwnerRepoGitTreesSha(owner, repo, sha, optional)
获取目录Tree

获取目录Tree

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **sha** | **string**| 可以是分支名(如master)、Commit或者目录Tree的SHA值 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **sha** | **string**| 可以是分支名(如master)、Commit或者目录Tree的SHA值 | 
 **accessToken** | **string**| 用户授权码 | 
 **recursive** | **int32**| 赋值为1递归获取目录 | 

### Return type

[**Tree**](Tree.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

