# gitee_client\GistsApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5GistsGistIdCommentsId**](GistsApi.md#DeleteV5GistsGistIdCommentsId) | **Delete** /v5/gists/{gist_id}/comments/{id} | 删除代码片段的评论
[**DeleteV5GistsId**](GistsApi.md#DeleteV5GistsId) | **Delete** /v5/gists/{id} | 删除该条代码片段
[**DeleteV5GistsIdStar**](GistsApi.md#DeleteV5GistsIdStar) | **Delete** /v5/gists/{id}/star | 取消Star代码片段
[**GetV5Gists**](GistsApi.md#GetV5Gists) | **Get** /v5/gists | 获取代码片段
[**GetV5GistsGistIdComments**](GistsApi.md#GetV5GistsGistIdComments) | **Get** /v5/gists/{gist_id}/comments | 获取代码片段的评论
[**GetV5GistsGistIdCommentsId**](GistsApi.md#GetV5GistsGistIdCommentsId) | **Get** /v5/gists/{gist_id}/comments/{id} | 获取单条代码片段的评论
[**GetV5GistsId**](GistsApi.md#GetV5GistsId) | **Get** /v5/gists/{id} | 获取单条代码片段
[**GetV5GistsIdCommits**](GistsApi.md#GetV5GistsIdCommits) | **Get** /v5/gists/{id}/commits | 获取代码片段的commit
[**GetV5GistsIdForks**](GistsApi.md#GetV5GistsIdForks) | **Get** /v5/gists/{id}/forks | 获取Fork该条代码片段的列表
[**GetV5GistsIdStar**](GistsApi.md#GetV5GistsIdStar) | **Get** /v5/gists/{id}/star | 判断代码片段是否已Star
[**GetV5GistsPublic**](GistsApi.md#GetV5GistsPublic) | **Get** /v5/gists/public | 获取公开的代码片段
[**GetV5GistsStarred**](GistsApi.md#GetV5GistsStarred) | **Get** /v5/gists/starred | 获取用户Star的代码片段
[**GetV5UsersUsernameGists**](GistsApi.md#GetV5UsersUsernameGists) | **Get** /v5/users/{username}/gists | 获取指定用户的公开代码片段
[**PatchV5GistsGistIdCommentsId**](GistsApi.md#PatchV5GistsGistIdCommentsId) | **Patch** /v5/gists/{gist_id}/comments/{id} | 修改代码片段的评论
[**PatchV5GistsId**](GistsApi.md#PatchV5GistsId) | **Patch** /v5/gists/{id} | 修改代码片段
[**PostV5Gists**](GistsApi.md#PostV5Gists) | **Post** /v5/gists | 创建代码片段
[**PostV5GistsGistIdComments**](GistsApi.md#PostV5GistsGistIdComments) | **Post** /v5/gists/{gist_id}/comments | 增加代码片段的评论
[**PostV5GistsIdForks**](GistsApi.md#PostV5GistsIdForks) | **Post** /v5/gists/{id}/forks | Fork代码片段
[**PutV5GistsIdStar**](GistsApi.md#PutV5GistsIdStar) | **Put** /v5/gists/{id}/star | Star代码片段


# **DeleteV5GistsGistIdCommentsId**
> DeleteV5GistsGistIdCommentsId(gistId, id, optional)
删除代码片段的评论

删除代码片段的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **gistId** | **string**| 代码片段的ID | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **string**| 代码片段的ID | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5GistsId**
> DeleteV5GistsId(id, optional)
删除该条代码片段

删除该条代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **DeleteV5GistsIdStar**
> DeleteV5GistsIdStar(id, optional)
取消Star代码片段

取消Star代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5Gists**
> []Code GetV5Gists(optional)
获取代码片段

获取代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Code**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsGistIdComments**
> []CodeComment GetV5GistsGistIdComments(gistId, optional)
获取代码片段的评论

获取代码片段的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **gistId** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsGistIdCommentsId**
> CodeComment GetV5GistsGistIdCommentsId(gistId, id, optional)
获取单条代码片段的评论

获取单条代码片段的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **gistId** | **string**| 代码片段的ID | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **string**| 代码片段的ID | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsId**
> CodeForksHistory GetV5GistsId(id, optional)
获取单条代码片段

获取单条代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsIdCommits**
> CodeForksHistory GetV5GistsIdCommits(id, optional)
获取代码片段的commit

获取代码片段的commit

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsIdForks**
> CodeForks GetV5GistsIdForks(id, optional)
获取Fork该条代码片段的列表

获取Fork该条代码片段的列表

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**CodeForks**](CodeForks.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsIdStar**
> GetV5GistsIdStar(id, optional)
判断代码片段是否已Star

判断代码片段是否已Star

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsPublic**
> []Code GetV5GistsPublic(optional)
获取公开的代码片段

获取公开的代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Code**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5GistsStarred**
> []Code GetV5GistsStarred(optional)
获取用户Star的代码片段

获取用户Star的代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Code**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UsersUsernameGists**
> []Code GetV5UsersUsernameGists(username, optional)
获取指定用户的公开代码片段

获取指定用户的公开代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **username** | **string**| 用户名(username/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **username** | **string**| 用户名(username/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Code**](Code.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5GistsGistIdCommentsId**
> CodeComment PatchV5GistsGistIdCommentsId(gistId, id, body, optional)
修改代码片段的评论

修改代码片段的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **gistId** | **string**| 代码片段的ID | 
  **id** | **int32**| 评论的ID | 
  **body** | **string**| 评论内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **string**| 代码片段的ID | 
 **id** | **int32**| 评论的ID | 
 **body** | **string**| 评论内容 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5GistsId**
> CodeForksHistory PatchV5GistsId(id, files, description, optional)
修改代码片段

修改代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
  **files** | [**map[string]string**](string.md)| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | 
  **description** | **string**| 代码片段描述，1~30个字符 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **files** | [**map[string]string**](string.md)| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | 
 **description** | **string**| 代码片段描述，1~30个字符 | 
 **accessToken** | **string**| 用户授权码 | 
 **public** | **bool**| 公开/私有，默认: 私有 | 

### Return type

[**CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5Gists**
> []CodeForksHistory PostV5Gists(files, description, optional)
创建代码片段

创建代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **files** | [**map[string]string**](string.md)| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | 
  **description** | **string**| 代码片段描述，1~30个字符 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **files** | [**map[string]string**](string.md)| Hash形式的代码片段文件名以及文件内容。如: { \&quot;file1.txt\&quot;: { \&quot;content\&quot;: \&quot;String file contents\&quot; } } | 
 **description** | **string**| 代码片段描述，1~30个字符 | 
 **accessToken** | **string**| 用户授权码 | 
 **public** | **bool**| 公开/私有，默认: 私有 | 

### Return type

[**[]CodeForksHistory**](CodeForksHistory.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5GistsGistIdComments**
> CodeComment PostV5GistsGistIdComments(gistId, body, optional)
增加代码片段的评论

增加代码片段的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **gistId** | **string**| 代码片段的ID | 
  **body** | **string**| 评论内容 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gistId** | **string**| 代码片段的ID | 
 **body** | **string**| 评论内容 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**CodeComment**](CodeComment.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5GistsIdForks**
> PostV5GistsIdForks(id, optional)
Fork代码片段

Fork代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PutV5GistsIdStar**
> PutV5GistsIdStar(id, optional)
Star代码片段

Star代码片段

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **string**| 代码片段的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**| 代码片段的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

