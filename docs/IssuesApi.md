# gitee_client\IssuesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#DeleteV5ReposOwnerRepoIssuesCommentsId) | **Delete** /v5/repos/{owner}/{repo}/issues/comments/{id} | 删除Issue某条评论
[**GetV5Issues**](IssuesApi.md#GetV5Issues) | **Get** /v5/issues | 获取当前授权用户的所有Issue
[**GetV5OrgsOrgIssues**](IssuesApi.md#GetV5OrgsOrgIssues) | **Get** /v5/orgs/{org}/issues | 获取当前用户某个组织的Issues
[**GetV5ReposOwnerRepoIssues**](IssuesApi.md#GetV5ReposOwnerRepoIssues) | **Get** /v5/repos/{owner}/{repo}/issues | 项目的所有Issues
[**GetV5ReposOwnerRepoIssuesComments**](IssuesApi.md#GetV5ReposOwnerRepoIssuesComments) | **Get** /v5/repos/{owner}/{repo}/issues/comments | 获取项目所有Issue的评论
[**GetV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#GetV5ReposOwnerRepoIssuesCommentsId) | **Get** /v5/repos/{owner}/{repo}/issues/comments/{id} | 获取项目Issue某条评论
[**GetV5ReposOwnerRepoIssuesNumber**](IssuesApi.md#GetV5ReposOwnerRepoIssuesNumber) | **Get** /v5/repos/{owner}/{repo}/issues/{number} | 项目的某个Issue
[**GetV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#GetV5ReposOwnerRepoIssuesNumberComments) | **Get** /v5/repos/{owner}/{repo}/issues/{number}/comments | 获取项目某个Issue所有的评论
[**GetV5UserIssues**](IssuesApi.md#GetV5UserIssues) | **Get** /v5/user/issues | 获取当前授权用户的所有Issues
[**PatchV5ReposOwnerRepoIssuesCommentsId**](IssuesApi.md#PatchV5ReposOwnerRepoIssuesCommentsId) | **Patch** /v5/repos/{owner}/{repo}/issues/comments/{id} | 更新Issue某条评论
[**PatchV5ReposOwnerRepoIssuesNumber**](IssuesApi.md#PatchV5ReposOwnerRepoIssuesNumber) | **Patch** /v5/repos/{owner}/{repo}/issues/{number} | 更新Issue
[**PostV5ReposOwnerRepoIssues**](IssuesApi.md#PostV5ReposOwnerRepoIssues) | **Post** /v5/repos/{owner}/{repo}/issues | 创建Issue
[**PostV5ReposOwnerRepoIssuesNumberComments**](IssuesApi.md#PostV5ReposOwnerRepoIssuesNumberComments) | **Post** /v5/repos/{owner}/{repo}/issues/{number}/comments | 创建某个Issue评论


# **DeleteV5ReposOwnerRepoIssuesCommentsId**
> DeleteV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, optional)
删除Issue某条评论

删除Issue某条评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5Issues**
> []Issue GetV5Issues(optional)
获取当前授权用户的所有Issue

获取当前授权用户的所有Issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **filter** | **string**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [default to assigned]
 **state** | **string**| Issue的状态: open, closed, or all。 默认: open | [default to open]
 **labels** | **string**| 用逗号分开的标签。如: bug,performance | 
 **sort** | **string**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [default to created]
 **direction** | **string**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [default to desc]
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5OrgsOrgIssues**
> []Issue GetV5OrgsOrgIssues(org, optional)
获取当前用户某个组织的Issues

获取当前用户某个组织的Issues

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **org** | **string**| 组织的路径(path/login) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org** | **string**| 组织的路径(path/login) | 
 **accessToken** | **string**| 用户授权码 | 
 **filter** | **string**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [default to assigned]
 **state** | **string**| Issue的状态: open, closed, or all。 默认: open | [default to open]
 **labels** | **string**| 用逗号分开的标签。如: bug,performance | 
 **sort** | **string**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [default to created]
 **direction** | **string**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [default to desc]
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssues**
> []Issue GetV5ReposOwnerRepoIssues(owner, repo, optional)
项目的所有Issues

项目的所有Issues

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| Issue的状态: open, closed, or all。 默认: open | [default to open]
 **labels** | **string**| 用逗号分开的标签。如: bug,performance | 
 **sort** | **string**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [default to created]
 **direction** | **string**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [default to desc]
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]
 **milestone** | **string**| 根据里程碑标题。none为没里程碑的，*为所有带里程碑的 | 
 **assignee** | **string**| 用户的username。 none为没指派者, *为所有带有指派者的 | 
 **creator** | **string**| 创建Issues的用户username | 

### Return type

[**[]Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssuesComments**
> GetV5ReposOwnerRepoIssuesComments(owner, repo, optional)
获取项目所有Issue的评论

获取项目所有Issue的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **sort** | **string**| Either created or updated. Default: created | [default to created]
 **direction** | **string**| Either asc or desc. Ignored without the sort parameter. | [default to asc]
 **since** | **string**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssuesCommentsId**
> GetV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, optional)
获取项目Issue某条评论

获取项目Issue某条评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssuesNumber**
> Issue GetV5ReposOwnerRepoIssuesNumber(owner, repo, number, optional)
项目的某个Issue

项目的某个Issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoIssuesNumberComments**
> GetV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, optional)
获取项目某个Issue所有的评论

获取项目某个Issue所有的评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **accessToken** | **string**| 用户授权码 | 
 **since** | **string**| Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5UserIssues**
> []Issue GetV5UserIssues(optional)
获取当前授权用户的所有Issues

获取当前授权用户的所有Issues

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accessToken** | **string**| 用户授权码 | 
 **filter** | **string**| 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned | [default to assigned]
 **state** | **string**| Issue的状态: open, closed, or all。 默认: open | [default to open]
 **labels** | **string**| 用逗号分开的标签。如: bug,performance | 
 **sort** | **string**| 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at | [default to created]
 **direction** | **string**| 排序方式: 升序(asc)，降序(desc)。默认: desc | [default to desc]
 **since** | **string**| 起始的更新时间，要求时间格式为 ISO 8601 | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoIssuesCommentsId**
> PatchV5ReposOwnerRepoIssuesCommentsId(owner, repo, id, body, optional)
更新Issue某条评论

更新Issue某条评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **id** | **int32**| 评论的ID | 
  **body** | **string**| The contents of the comment. | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **id** | **int32**| 评论的ID | 
 **body** | **string**| The contents of the comment. | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoIssuesNumber**
> Issue PatchV5ReposOwnerRepoIssuesNumber(owner, repo, number, title, optional)
更新Issue

更新Issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
  **title** | **string**| Issue标题 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **title** | **string**| Issue标题 | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| Issue 状态，open、started、closed 或 approved | 
 **body** | **string**| Issue描述 | 
 **assignee** | **string**| Issue负责人的username | 
 **milestone** | **int32**| 所属里程碑的number(第几个) | 
 **labels** | [**[]string**](string.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoIssues**
> Issue PostV5ReposOwnerRepoIssues(owner, repo, title, optional)
创建Issue

创建Issue

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **title** | **string**| Issue标题 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **title** | **string**| Issue标题 | 
 **accessToken** | **string**| 用户授权码 | 
 **body** | **string**| Issue描述 | 
 **assignee** | **string**| Issue负责人的username | 
 **milestone** | **int32**| 所属里程碑的number(第几个) | 
 **labels** | [**[]string**](string.md)| 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance | 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoIssuesNumberComments**
> PostV5ReposOwnerRepoIssuesNumberComments(owner, repo, number, body, optional)
创建某个Issue评论

创建某个Issue评论

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **string**| Issue 编号(区分大小写) | 
  **body** | **string**| The contents of the comment. | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **string**| Issue 编号(区分大小写) | 
 **body** | **string**| The contents of the comment. | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

