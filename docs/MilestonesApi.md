# gitee_client\MilestonesApi

All URIs are relative to *https://gitee.com/api*

Method | HTTP request | Description
------------- | ------------- | -------------
[**DeleteV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#DeleteV5ReposOwnerRepoMilestonesNumber) | **Delete** /v5/repos/{owner}/{repo}/milestones/{number} | 删除项目单个里程碑
[**GetV5ReposOwnerRepoMilestones**](MilestonesApi.md#GetV5ReposOwnerRepoMilestones) | **Get** /v5/repos/{owner}/{repo}/milestones | 获取项目所有里程碑
[**GetV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#GetV5ReposOwnerRepoMilestonesNumber) | **Get** /v5/repos/{owner}/{repo}/milestones/{number} | 获取项目单个里程碑
[**PatchV5ReposOwnerRepoMilestonesNumber**](MilestonesApi.md#PatchV5ReposOwnerRepoMilestonesNumber) | **Patch** /v5/repos/{owner}/{repo}/milestones/{number} | 更新项目里程碑
[**PostV5ReposOwnerRepoMilestones**](MilestonesApi.md#PostV5ReposOwnerRepoMilestones) | **Post** /v5/repos/{owner}/{repo}/milestones | 创建项目里程碑


# **DeleteV5ReposOwnerRepoMilestonesNumber**
> DeleteV5ReposOwnerRepoMilestonesNumber(owner, repo, number, optional)
删除项目单个里程碑

删除项目单个里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoMilestones**
> []Milestone GetV5ReposOwnerRepoMilestones(owner, repo, optional)
获取项目所有里程碑

获取项目所有里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| 里程碑状态: open, closed, all。默认: open | [default to open]
 **sort** | **string**| 排序方式: due_on | [default to due_on]
 **direction** | **string**| 升序(asc)或是降序(desc)。默认: asc | 
 **page** | **int32**| 当前的页码 | [default to 1]
 **perPage** | **int32**| 每页的数量 | [default to 20]

### Return type

[**[]Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **GetV5ReposOwnerRepoMilestonesNumber**
> Milestone GetV5ReposOwnerRepoMilestonesNumber(owner, repo, number, optional)
获取项目单个里程碑

获取项目单个里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
 **accessToken** | **string**| 用户授权码 | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PatchV5ReposOwnerRepoMilestonesNumber**
> Milestone PatchV5ReposOwnerRepoMilestonesNumber(owner, repo, number, title, optional)
更新项目里程碑

更新项目里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
  **title** | **string**| 里程碑标题 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **number** | **int32**| 第几个里程碑，即本项目里程碑的序数 | 
 **title** | **string**| 里程碑标题 | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| 里程碑状态: open, closed, all。默认: open | [default to open]
 **description** | **string**| 里程碑具体描述 | 
 **dueOn** | **string**| 里程碑的截止日期 | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **PostV5ReposOwnerRepoMilestones**
> Milestone PostV5ReposOwnerRepoMilestones(owner, repo, title, optional)
创建项目里程碑

创建项目里程碑

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **owner** | **string**| 用户名(username/login) | 
  **repo** | **string**| 项目路径(path) | 
  **title** | **string**| 里程碑标题 | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **owner** | **string**| 用户名(username/login) | 
 **repo** | **string**| 项目路径(path) | 
 **title** | **string**| 里程碑标题 | 
 **accessToken** | **string**| 用户授权码 | 
 **state** | **string**| 里程碑状态: open, closed, all。默认: open | [default to open]
 **description** | **string**| 里程碑具体描述 | 
 **dueOn** | **string**| 里程碑的截止日期 | 

### Return type

[**Milestone**](Milestone.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

