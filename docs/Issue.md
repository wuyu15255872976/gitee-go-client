# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**RepositoryUrl** | **string** |  | [optional] [default to null]
**LabelsUrl** | **string** |  | [optional] [default to null]
**EventsUrl** | **string** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]
**Number** | **string** |  | [optional] [default to null]
**State** | **string** |  | [optional] [default to null]
**Title** | **string** |  | [optional] [default to null]
**Body** | **string** |  | [optional] [default to null]
**User** | **string** |  | [optional] [default to null]
**Labels** | [***Label**](Label.md) |  | [optional] [default to null]
**Assignee** | **string** |  | [optional] [default to null]
**Repository** | **string** |  | [optional] [default to null]
**Milestone** | [***Milestone**](Milestone.md) |  | [optional] [default to null]
**CreatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**UpdatedAt** | [**time.Time**](time.Time.md) |  | [optional] [default to null]
**Comments** | **int32** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


