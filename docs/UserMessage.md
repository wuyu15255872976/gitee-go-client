# UserMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **string** |  | [optional] [default to null]
**Sender** | **string** |  | [optional] [default to null]
**Unread** | **string** |  | [optional] [default to null]
**Content** | **string** |  | [optional] [default to null]
**UpdatedAt** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


