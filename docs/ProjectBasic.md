# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **int32** |  | [optional] [default to null]
**FullName** | **string** |  | [optional] [default to null]
**Url** | **string** |  | [optional] [default to null]
**Path** | **string** |  | [optional] [default to null]
**Name** | **string** |  | [optional] [default to null]
**Owner** | **string** |  | [optional] [default to null]
**Description** | **string** |  | [optional] [default to null]
**Private** | **string** |  | [optional] [default to null]
**Fork** | **string** |  | [optional] [default to null]
**HtmlUrl** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


