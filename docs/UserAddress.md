# UserAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Name** | **string** |  | [optional] [default to null]
**Tel** | **string** |  | [optional] [default to null]
**Address** | **string** |  | [optional] [default to null]
**Province** | **string** |  | [optional] [default to null]
**City** | **string** |  | [optional] [default to null]
**ZipCode** | **string** |  | [optional] [default to null]
**Comment** | **string** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


