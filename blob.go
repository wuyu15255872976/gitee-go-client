/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取文件Blob
type Blob struct {

	Sha string `json:"sha,omitempty"`

	Size string `json:"size,omitempty"`

	Url string `json:"url,omitempty"`

	Content string `json:"content,omitempty"`

	Encoding string `json:"encoding,omitempty"`
}
