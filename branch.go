/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取所有分支
type Branch struct {

	Name string `json:"name,omitempty"`

	Commit string `json:"commit,omitempty"`

	Protected string `json:"protected,omitempty"`

	ProtectionUrl string `json:"protection_url,omitempty"`
}
