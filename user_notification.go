/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取一个通知
type UserNotification struct {

	Id string `json:"id,omitempty"`

	Repository *ProjectBasic `json:"repository,omitempty"`

	Unread string `json:"unread,omitempty"`

	Mute string `json:"mute,omitempty"`

	Subject string `json:"subject,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	Url string `json:"url,omitempty"`
}
