/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取目录Tree
type Tree struct {

	Sha string `json:"sha,omitempty"`

	Url string `json:"url,omitempty"`

	Tree string `json:"tree,omitempty"`

	Truncated string `json:"truncated,omitempty"`
}
