/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 

 */

package gitee_client

// 更新授权用户的地理信息
type UserDetail struct {

	Login string `json:"login,omitempty"`

	Id int32 `json:"id,omitempty"`

	AvatarUrl string `json:"avatar_url,omitempty"`

	Url string `json:"url,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	FollowersUrl string `json:"followers_url,omitempty"`

	FollowingUrl string `json:"following_url,omitempty"`

	GistsUrl string `json:"gists_url,omitempty"`

	StarredUrl string `json:"starred_url,omitempty"`

	SubscriptionsUrl string `json:"subscriptions_url,omitempty"`

	OrganizationsUrl string `json:"organizations_url,omitempty"`

	ReposUrl string `json:"repos_url,omitempty"`

	EventsUrl string `json:"events_url,omitempty"`

	ReceivedEventsUrl string `json:"received_events_url,omitempty"`

	Type_ string `json:"type,omitempty"`

	SiteAdmin string `json:"site_admin,omitempty"`

	Name string `json:"name,omitempty"`

	Blog string `json:"blog,omitempty"`

	Weibo string `json:"weibo,omitempty"`

	Bio string `json:"bio,omitempty"`

	PublicRepos string `json:"public_repos,omitempty"`

	PublicGists string `json:"public_gists,omitempty"`

	Followers string `json:"followers,omitempty"`

	Following string `json:"following,omitempty"`

	Stared string `json:"stared,omitempty"`

	Watched string `json:"watched,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	Email string `json:"email,omitempty"`

	UnconfirmedEmail string `json:"unconfirmed_email,omitempty"`

	Phone string `json:"phone,omitempty"`

	PrivateToken string `json:"private_token,omitempty"`

	TotalRepos string `json:"total_repos,omitempty"`

	OwnedRepos string `json:"owned_repos,omitempty"`

	TotalPrivateRepos string `json:"total_private_repos,omitempty"`

	OwnedPrivateRepos string `json:"owned_private_repos,omitempty"`

	PrivateGists string `json:"private_gists,omitempty"`

	Address *UserAddress `json:"address,omitempty"`
}
