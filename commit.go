/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

type Commit struct {

	Sha string `json:"sha,omitempty"`

	Author string `json:"author,omitempty"`

	Committer string `json:"committer,omitempty"`

	Message string `json:"message,omitempty"`

	Tree string `json:"tree,omitempty"`

	Parents string `json:"parents,omitempty"`
}
