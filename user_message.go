/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取一个私信
type UserMessage struct {

	Id string `json:"id,omitempty"`

	Sender string `json:"sender,omitempty"`

	Unread string `json:"unread,omitempty"`

	Content string `json:"content,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	Url string `json:"url,omitempty"`
}
