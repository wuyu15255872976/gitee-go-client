/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

import (
	"time"
)

// 更新项目里程碑
type Milestone struct {

	Url string `json:"url,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	Id int32 `json:"id,omitempty"`

	Number int32 `json:"number,omitempty"`

	RepositoryId int32 `json:"repository_id,omitempty"`

	State string `json:"state,omitempty"`

	Title string `json:"title,omitempty"`

	Description string `json:"description,omitempty"`

	UpdatedAt time.Time `json:"updated_at,omitempty"`

	CreatedAt time.Time `json:"created_at,omitempty"`

	OpenIssues int32 `json:"open_issues,omitempty"`

	StartedIssues int32 `json:"started_issues,omitempty"`

	ClosedIssues int32 `json:"closed_issues,omitempty"`

	ApprovedIssues int32 `json:"approved_issues,omitempty"`

	DueOn time.Time `json:"due_on,omitempty"`
}
