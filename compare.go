/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 两个Commits之间对比的版本差异
type Compare struct {

	BaseCommit string `json:"base_commit,omitempty"`

	MergeBaseCommit string `json:"merge_base_commit,omitempty"`

	Commits string `json:"commits,omitempty"`

	Files string `json:"files,omitempty"`
}
