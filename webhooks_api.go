/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

import (
	"net/url"
	"net/http"
	"strings"
	"golang.org/x/net/context"
	"encoding/json"
	"fmt"
)

// Linger please
var (
	_ context.Context
)

type WebhooksApiService service


/* WebhooksApiService 删除一个项目WebHook
 删除一个项目WebHook

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id Webhook的ID
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *WebhooksApiService) DeleteV5ReposOwnerRepoHooksId(owner string, repo string, id int32, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Delete")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* WebhooksApiService 列出项目的WebHooks
 列出项目的WebHooks

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return []Hook*/
func (a *WebhooksApiService) GetV5ReposOwnerRepoHooks(owner string, repo string, localVarOptionals map[string]interface{}) ([]Hook,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  []Hook
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* WebhooksApiService 获取项目单个WebHook
 获取项目单个WebHook

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id Webhook的ID
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return Hook*/
func (a *WebhooksApiService) GetV5ReposOwnerRepoHooksId(owner string, repo string, id int32, localVarOptionals map[string]interface{}) (Hook,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Hook
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* WebhooksApiService 更新一个项目WebHook
 更新一个项目WebHook

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id Webhook的ID
 @param url 远程HTTP URL
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "password" (string) 请求URL时会带上该密码，防止URL被恶意请求
     @param "pushEvents" (bool) Push代码到仓库
     @param "tagPushEvents" (bool) 提交Tag到仓库
     @param "issuesEvents" (bool) 创建/关闭Issue
     @param "noteEvents" (bool) 评论了Issue/代码等等
     @param "mergeRequestsEvents" (bool) 合并请求和合并后
 @return Hook*/
func (a *WebhooksApiService) PatchV5ReposOwnerRepoHooksId(owner string, repo string, id int32, url string, localVarOptionals map[string]interface{}) (Hook,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Patch")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Hook
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["password"], "string", "password"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["pushEvents"], "bool", "pushEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["tagPushEvents"], "bool", "tagPushEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["issuesEvents"], "bool", "issuesEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["noteEvents"], "bool", "noteEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["mergeRequestsEvents"], "bool", "mergeRequestsEvents"); err != nil {
		return successPayload, nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("url", parameterToString(url, ""))
	if localVarTempParam, localVarOk := localVarOptionals["password"].(string); localVarOk {
		localVarFormParams.Add("password", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["pushEvents"].(bool); localVarOk {
		localVarFormParams.Add("push_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["tagPushEvents"].(bool); localVarOk {
		localVarFormParams.Add("tag_push_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["issuesEvents"].(bool); localVarOk {
		localVarFormParams.Add("issues_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["noteEvents"].(bool); localVarOk {
		localVarFormParams.Add("note_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["mergeRequestsEvents"].(bool); localVarOk {
		localVarFormParams.Add("merge_requests_events", parameterToString(localVarTempParam, ""))
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* WebhooksApiService 创建一个项目WebHook
 创建一个项目WebHook

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param url 远程HTTP URL
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "password" (string) 请求URL时会带上该密码，防止URL被恶意请求
     @param "pushEvents" (bool) Push代码到仓库
     @param "tagPushEvents" (bool) 提交Tag到仓库
     @param "issuesEvents" (bool) 创建/关闭Issue
     @param "noteEvents" (bool) 评论了Issue/代码等等
     @param "mergeRequestsEvents" (bool) 合并请求和合并后
 @return Hook*/
func (a *WebhooksApiService) PostV5ReposOwnerRepoHooks(owner string, repo string, url string, localVarOptionals map[string]interface{}) (Hook,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Post")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Hook
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["password"], "string", "password"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["pushEvents"], "bool", "pushEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["tagPushEvents"], "bool", "tagPushEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["issuesEvents"], "bool", "issuesEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["noteEvents"], "bool", "noteEvents"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["mergeRequestsEvents"], "bool", "mergeRequestsEvents"); err != nil {
		return successPayload, nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{ "application/json",  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("url", parameterToString(url, ""))
	if localVarTempParam, localVarOk := localVarOptionals["password"].(string); localVarOk {
		localVarFormParams.Add("password", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["pushEvents"].(bool); localVarOk {
		localVarFormParams.Add("push_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["tagPushEvents"].(bool); localVarOk {
		localVarFormParams.Add("tag_push_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["issuesEvents"].(bool); localVarOk {
		localVarFormParams.Add("issues_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["noteEvents"].(bool); localVarOk {
		localVarFormParams.Add("note_events", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["mergeRequestsEvents"].(bool); localVarOk {
		localVarFormParams.Add("merge_requests_events", parameterToString(localVarTempParam, ""))
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* WebhooksApiService 测试WebHook是否发送成功
 测试WebHook是否发送成功

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id Webhook的ID
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *WebhooksApiService) PostV5ReposOwnerRepoHooksIdTests(owner string, repo string, id int32, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Post")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/hooks/{id}/tests"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{ "application/json",  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

