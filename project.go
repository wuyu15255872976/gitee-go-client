/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 

 */

package gitee_client

// 获取一个组织的项目
type Project struct {

	Id int32 `json:"id,omitempty"`

	FullName string `json:"full_name,omitempty"`

	Url string `json:"url,omitempty"`

	Path string `json:"path,omitempty"`

	Name string `json:"name,omitempty"`

	Owner string `json:"owner,omitempty"`

	Description string `json:"description,omitempty"`

	Private string `json:"private,omitempty"`

	Fork string `json:"fork,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	ForksUrl string `json:"forks_url,omitempty"`

	KeysUrl string `json:"keys_url,omitempty"`

	CollaboratorsUrl string `json:"collaborators_url,omitempty"`

	HooksUrl string `json:"hooks_url,omitempty"`

	BranchesUrl string `json:"branches_url,omitempty"`

	TagsUrl string `json:"tags_url,omitempty"`

	BlobsUrl string `json:"blobs_url,omitempty"`

	StargazersUrl string `json:"stargazers_url,omitempty"`

	ContributorsUrl string `json:"contributors_url,omitempty"`

	CommitsUrl string `json:"commits_url,omitempty"`

	CommentsUrl string `json:"comments_url,omitempty"`

	IssueCommentUrl string `json:"issue_comment_url,omitempty"`

	IssuesUrl string `json:"issues_url,omitempty"`

	PullsUrl string `json:"pulls_url,omitempty"`

	MilestonesUrl string `json:"milestones_url,omitempty"`

	NotificationsUrl string `json:"notifications_url,omitempty"`

	LabelsUrl string `json:"labels_url,omitempty"`

	ReleasesUrl string `json:"releases_url,omitempty"`

	Recommend string `json:"recommend,omitempty"`

	Homepage string `json:"homepage,omitempty"`

	Language string `json:"language,omitempty"`

	ForksCount string `json:"forks_count,omitempty"`

	StargazersCount string `json:"stargazers_count,omitempty"`

	WatchersCount string `json:"watchers_count,omitempty"`

	DefaultBranch string `json:"default_branch,omitempty"`

	OpenIssuesCount int32 `json:"open_issues_count,omitempty"`

	HasIssues string `json:"has_issues,omitempty"`

	HasWiki string `json:"has_wiki,omitempty"`

	PullRequestsEnabled string `json:"pull_requests_enabled,omitempty"`

	HasPage string `json:"has_page,omitempty"`

	License string `json:"license,omitempty"`

	PushedAt string `json:"pushed_at,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	Parent *Project `json:"parent,omitempty"`

	Paas string `json:"paas,omitempty"`

	Stared string `json:"stared,omitempty"`

	Watched string `json:"watched,omitempty"`

	Permission string `json:"permission,omitempty"`

	Relation string `json:"relation,omitempty"`
}
