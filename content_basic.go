/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

type ContentBasic struct {

	Name string `json:"name,omitempty"`

	Path string `json:"path,omitempty"`

	Size string `json:"size,omitempty"`

	Sha string `json:"sha,omitempty"`

	Type_ string `json:"type,omitempty"`

	Url string `json:"url,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	DownloadUrl string `json:"download_url,omitempty"`

	Links string `json:"_links,omitempty"`
}
