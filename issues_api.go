/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

import (
	"net/url"
	"net/http"
	"strings"
	"golang.org/x/net/context"
	"encoding/json"
	"fmt"
)

// Linger please
var (
	_ context.Context
)

type IssuesApiService service


/* IssuesApiService 删除Issue某条评论
 删除Issue某条评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id 评论的ID
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *IssuesApiService) DeleteV5ReposOwnerRepoIssuesCommentsId(owner string, repo string, id int32, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Delete")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/comments/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* IssuesApiService 获取当前授权用户的所有Issue
 获取当前授权用户的所有Issue

 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "filter" (string) 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
     @param "state" (string) Issue的状态: open, closed, or all。 默认: open
     @param "labels" (string) 用逗号分开的标签。如: bug,performance
     @param "sort" (string) 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
     @param "direction" (string) 排序方式: 升序(asc)，降序(desc)。默认: desc
     @param "since" (string) 起始的更新时间，要求时间格式为 ISO 8601
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return []Issue*/
func (a *IssuesApiService) GetV5Issues(localVarOptionals map[string]interface{}) ([]Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  []Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/issues"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["filter"], "string", "filter"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["state"], "string", "state"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["labels"], "string", "labels"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["sort"], "string", "sort"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["direction"], "string", "direction"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["filter"].(string); localVarOk {
		localVarQueryParams.Add("filter", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["state"].(string); localVarOk {
		localVarQueryParams.Add("state", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].(string); localVarOk {
		localVarQueryParams.Add("labels", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["sort"].(string); localVarOk {
		localVarQueryParams.Add("sort", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["direction"].(string); localVarOk {
		localVarQueryParams.Add("direction", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 获取当前用户某个组织的Issues
 获取当前用户某个组织的Issues

 @param org 组织的路径(path/login)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "filter" (string) 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
     @param "state" (string) Issue的状态: open, closed, or all。 默认: open
     @param "labels" (string) 用逗号分开的标签。如: bug,performance
     @param "sort" (string) 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
     @param "direction" (string) 排序方式: 升序(asc)，降序(desc)。默认: desc
     @param "since" (string) 起始的更新时间，要求时间格式为 ISO 8601
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return []Issue*/
func (a *IssuesApiService) GetV5OrgsOrgIssues(org string, localVarOptionals map[string]interface{}) ([]Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  []Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/orgs/{org}/issues"
	localVarPath = strings.Replace(localVarPath, "{"+"org"+"}", fmt.Sprintf("%v", org), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["filter"], "string", "filter"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["state"], "string", "state"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["labels"], "string", "labels"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["sort"], "string", "sort"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["direction"], "string", "direction"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["filter"].(string); localVarOk {
		localVarQueryParams.Add("filter", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["state"].(string); localVarOk {
		localVarQueryParams.Add("state", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].(string); localVarOk {
		localVarQueryParams.Add("labels", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["sort"].(string); localVarOk {
		localVarQueryParams.Add("sort", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["direction"].(string); localVarOk {
		localVarQueryParams.Add("direction", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 项目的所有Issues
 项目的所有Issues

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "state" (string) Issue的状态: open, closed, or all。 默认: open
     @param "labels" (string) 用逗号分开的标签。如: bug,performance
     @param "sort" (string) 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
     @param "direction" (string) 排序方式: 升序(asc)，降序(desc)。默认: desc
     @param "since" (string) 起始的更新时间，要求时间格式为 ISO 8601
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
     @param "milestone" (string) 根据里程碑标题。none为没里程碑的，*为所有带里程碑的
     @param "assignee" (string) 用户的username。 none为没指派者, *为所有带有指派者的
     @param "creator" (string) 创建Issues的用户username
 @return []Issue*/
func (a *IssuesApiService) GetV5ReposOwnerRepoIssues(owner string, repo string, localVarOptionals map[string]interface{}) ([]Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  []Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["state"], "string", "state"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["labels"], "string", "labels"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["sort"], "string", "sort"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["direction"], "string", "direction"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["milestone"], "string", "milestone"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["assignee"], "string", "assignee"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["creator"], "string", "creator"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["state"].(string); localVarOk {
		localVarQueryParams.Add("state", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].(string); localVarOk {
		localVarQueryParams.Add("labels", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["sort"].(string); localVarOk {
		localVarQueryParams.Add("sort", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["direction"].(string); localVarOk {
		localVarQueryParams.Add("direction", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["milestone"].(string); localVarOk {
		localVarQueryParams.Add("milestone", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["assignee"].(string); localVarOk {
		localVarQueryParams.Add("assignee", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["creator"].(string); localVarOk {
		localVarQueryParams.Add("creator", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 获取项目所有Issue的评论
 获取项目所有Issue的评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "sort" (string) Either created or updated. Default: created
     @param "direction" (string) Either asc or desc. Ignored without the sort parameter.
     @param "since" (string) Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return */
func (a *IssuesApiService) GetV5ReposOwnerRepoIssuesComments(owner string, repo string, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/comments"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["sort"], "string", "sort"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["direction"], "string", "direction"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["sort"].(string); localVarOk {
		localVarQueryParams.Add("sort", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["direction"].(string); localVarOk {
		localVarQueryParams.Add("direction", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* IssuesApiService 获取项目Issue某条评论
 获取项目Issue某条评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id 评论的ID
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *IssuesApiService) GetV5ReposOwnerRepoIssuesCommentsId(owner string, repo string, id int32, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/comments/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* IssuesApiService 项目的某个Issue
 项目的某个Issue

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param number Issue 编号(区分大小写)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return Issue*/
func (a *IssuesApiService) GetV5ReposOwnerRepoIssuesNumber(owner string, repo string, number string, localVarOptionals map[string]interface{}) (Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/{number}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"number"+"}", fmt.Sprintf("%v", number), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 获取项目某个Issue所有的评论
 获取项目某个Issue所有的评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param number Issue 编号(区分大小写)
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "since" (string) Only comments updated at or after this time are returned.                                               This is a timestamp in ISO 8601 format: YYYY-MM-DDTHH:MM:SSZ
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return */
func (a *IssuesApiService) GetV5ReposOwnerRepoIssuesNumberComments(owner string, repo string, number string, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/{number}/comments"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"number"+"}", fmt.Sprintf("%v", number), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* IssuesApiService 获取当前授权用户的所有Issues
 获取当前授权用户的所有Issues

 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "filter" (string) 筛选参数: 授权用户负责的(assigned)，授权用户创建的(created)，包含前两者的(all)。默认: assigned
     @param "state" (string) Issue的状态: open, closed, or all。 默认: open
     @param "labels" (string) 用逗号分开的标签。如: bug,performance
     @param "sort" (string) 排序依据: 创建时间(created)，更新时间(updated_at)。默认: created_at
     @param "direction" (string) 排序方式: 升序(asc)，降序(desc)。默认: desc
     @param "since" (string) 起始的更新时间，要求时间格式为 ISO 8601
     @param "page" (int32) 当前的页码
     @param "perPage" (int32) 每页的数量
 @return []Issue*/
func (a *IssuesApiService) GetV5UserIssues(localVarOptionals map[string]interface{}) ([]Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Get")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  []Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/user/issues"

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["filter"], "string", "filter"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["state"], "string", "state"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["labels"], "string", "labels"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["sort"], "string", "sort"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["direction"], "string", "direction"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["since"], "string", "since"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["page"], "int32", "page"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["perPage"], "int32", "perPage"); err != nil {
		return successPayload, nil, err
	}

	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarQueryParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["filter"].(string); localVarOk {
		localVarQueryParams.Add("filter", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["state"].(string); localVarOk {
		localVarQueryParams.Add("state", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].(string); localVarOk {
		localVarQueryParams.Add("labels", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["sort"].(string); localVarOk {
		localVarQueryParams.Add("sort", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["direction"].(string); localVarOk {
		localVarQueryParams.Add("direction", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["since"].(string); localVarOk {
		localVarQueryParams.Add("since", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["page"].(int32); localVarOk {
		localVarQueryParams.Add("page", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["perPage"].(int32); localVarOk {
		localVarQueryParams.Add("per_page", parameterToString(localVarTempParam, ""))
	}
	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 更新Issue某条评论
 更新Issue某条评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param id 评论的ID
 @param body The contents of the comment.
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *IssuesApiService) PatchV5ReposOwnerRepoIssuesCommentsId(owner string, repo string, id int32, body string, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Patch")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/comments/{id}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"id"+"}", fmt.Sprintf("%v", id), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("body", parameterToString(body, ""))
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

/* IssuesApiService 更新Issue
 更新Issue

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param number Issue 编号(区分大小写)
 @param title Issue标题
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "state" (string) Issue 状态，open、started、closed 或 approved
     @param "body" (string) Issue描述
     @param "assignee" (string) Issue负责人的username
     @param "milestone" (int32) 所属里程碑的number(第几个)
     @param "labels" ([]string) 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance
 @return Issue*/
func (a *IssuesApiService) PatchV5ReposOwnerRepoIssuesNumber(owner string, repo string, number string, title string, localVarOptionals map[string]interface{}) (Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Patch")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/{number}"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"number"+"}", fmt.Sprintf("%v", number), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["state"], "string", "state"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["body"], "string", "body"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["assignee"], "string", "assignee"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["milestone"], "int32", "milestone"); err != nil {
		return successPayload, nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["state"].(string); localVarOk {
		localVarFormParams.Add("state", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("title", parameterToString(title, ""))
	if localVarTempParam, localVarOk := localVarOptionals["body"].(string); localVarOk {
		localVarFormParams.Add("body", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["assignee"].(string); localVarOk {
		localVarFormParams.Add("assignee", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["milestone"].(int32); localVarOk {
		localVarFormParams.Add("milestone", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].([]string); localVarOk {
		localVarFormParams.Add("labels", parameterToString(localVarTempParam, "csv"))
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 创建Issue
 创建Issue

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param title Issue标题
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
     @param "body" (string) Issue描述
     @param "assignee" (string) Issue负责人的username
     @param "milestone" (int32) 所属里程碑的number(第几个)
     @param "labels" ([]string) 标签。如: [\&quot;bug\&quot;,\&quot;performance\&quot;]。此处试验直接对标签名换行即可，如: bug performance
 @return Issue*/
func (a *IssuesApiService) PostV5ReposOwnerRepoIssues(owner string, repo string, title string, localVarOptionals map[string]interface{}) (Issue,  *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Post")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	 	successPayload  Issue
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["body"], "string", "body"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["assignee"], "string", "assignee"); err != nil {
		return successPayload, nil, err
	}
	if err := typeCheckParameter(localVarOptionals["milestone"], "int32", "milestone"); err != nil {
		return successPayload, nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{ "application/json",  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("title", parameterToString(title, ""))
	if localVarTempParam, localVarOk := localVarOptionals["body"].(string); localVarOk {
		localVarFormParams.Add("body", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["assignee"].(string); localVarOk {
		localVarFormParams.Add("assignee", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["milestone"].(int32); localVarOk {
		localVarFormParams.Add("milestone", parameterToString(localVarTempParam, ""))
	}
	if localVarTempParam, localVarOk := localVarOptionals["labels"].([]string); localVarOk {
		localVarFormParams.Add("labels", parameterToString(localVarTempParam, "csv"))
	}
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return successPayload, nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return successPayload, localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return successPayload, localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }
	
	if err = json.NewDecoder(localVarHttpResponse.Body).Decode(&successPayload); err != nil {
	 	return successPayload, localVarHttpResponse, err
	}


	return successPayload, localVarHttpResponse, err
}

/* IssuesApiService 创建某个Issue评论
 创建某个Issue评论

 @param owner 用户名(username/login)
 @param repo 项目路径(path)
 @param number Issue 编号(区分大小写)
 @param body The contents of the comment.
 @param optional (nil or map[string]interface{}) with one or more of:
     @param "accessToken" (string) 用户授权码
 @return */
func (a *IssuesApiService) PostV5ReposOwnerRepoIssuesNumberComments(owner string, repo string, number string, body string, localVarOptionals map[string]interface{}) ( *http.Response, error) {
	var (
		localVarHttpMethod = strings.ToUpper("Post")
		localVarPostBody interface{}
		localVarFileName string
		localVarFileBytes []byte
	)

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/v5/repos/{owner}/{repo}/issues/{number}/comments"
	localVarPath = strings.Replace(localVarPath, "{"+"owner"+"}", fmt.Sprintf("%v", owner), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"repo"+"}", fmt.Sprintf("%v", repo), -1)
	localVarPath = strings.Replace(localVarPath, "{"+"number"+"}", fmt.Sprintf("%v", number), -1)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	if err := typeCheckParameter(localVarOptionals["accessToken"], "string", "accessToken"); err != nil {
		return nil, err
	}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{ "application/json",  }

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
		}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}
	if localVarTempParam, localVarOk := localVarOptionals["accessToken"].(string); localVarOk {
		localVarFormParams.Add("access_token", parameterToString(localVarTempParam, ""))
	}
	localVarFormParams.Add("body", parameterToString(body, ""))
	r, err := a.client.prepareRequest(nil, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	 localVarHttpResponse, err := a.client.callAPI(r)
	 if err != nil || localVarHttpResponse == nil {
		  return localVarHttpResponse, err
	 }
	 defer localVarHttpResponse.Body.Close()
	 if localVarHttpResponse.StatusCode >= 300 {
		return localVarHttpResponse, reportError(localVarHttpResponse.Status)
	 }

	return localVarHttpResponse, err
}

