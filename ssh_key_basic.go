/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 列出指定用户的所有公钥
type SshKeyBasic struct {

	Id string `json:"id,omitempty"`

	Key string `json:"key,omitempty"`
}
