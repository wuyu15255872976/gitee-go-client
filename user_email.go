/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 添加授权用户的新邮箱地址
type UserEmail struct {

	Email string `json:"email,omitempty"`

	UnconfirmedEmail string `json:"unconfirmed_email,omitempty"`
}
