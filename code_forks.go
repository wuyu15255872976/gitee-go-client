/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 获取Fork该条代码片段的列表
type CodeForks struct {

	User string `json:"user,omitempty"`

	Url string `json:"url,omitempty"`

	Id string `json:"id,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`
}
