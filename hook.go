/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 更新一个项目WebHook
type Hook struct {

	Id string `json:"id,omitempty"`

	Url string `json:"url,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	Password string `json:"password,omitempty"`

	ProjectId string `json:"project_id,omitempty"`

	Result string `json:"result,omitempty"`

	ResultCode string `json:"result_code,omitempty"`

	PushEvents string `json:"push_events,omitempty"`

	TagPushEvents string `json:"tag_push_events,omitempty"`

	IssuesEvents string `json:"issues_events,omitempty"`

	NoteEvents string `json:"note_events,omitempty"`

	MergeRequestsEvents string `json:"merge_requests_events,omitempty"`
}
