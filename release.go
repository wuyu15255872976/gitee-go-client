/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

import (
	"time"
)

// 更新项目Release
type Release struct {

	Id int32 `json:"id,omitempty"`

	TagName string `json:"tag_name,omitempty"`

	TargetCommitish string `json:"target_commitish,omitempty"`

	Prerelease string `json:"prerelease,omitempty"`

	Name string `json:"name,omitempty"`

	Body string `json:"body,omitempty"`

	Author string `json:"author,omitempty"`

	CreatedAt time.Time `json:"created_at,omitempty"`

	Assets string `json:"assets,omitempty"`
}
