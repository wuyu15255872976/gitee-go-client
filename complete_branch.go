/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 设置分支保护
type CompleteBranch struct {

	Name string `json:"name,omitempty"`

	Commit string `json:"commit,omitempty"`

	Links string `json:"_links,omitempty"`

	Protected string `json:"protected,omitempty"`

	ProtectionUrl string `json:"protection_url,omitempty"`
}
