/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

type ProjectBasic struct {

	Id int32 `json:"id,omitempty"`

	FullName string `json:"full_name,omitempty"`

	Url string `json:"url,omitempty"`

	Path string `json:"path,omitempty"`

	Name string `json:"name,omitempty"`

	Owner string `json:"owner,omitempty"`

	Description string `json:"description,omitempty"`

	Private string `json:"private,omitempty"`

	Fork string `json:"fork,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`
}
