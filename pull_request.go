/* 
 * 码云 Open API
 *
 *
 *
 * OpenAPI spec version: 5.0.1
 * 
 *
 */

package gitee_client

// 增加审查人员
type PullRequest struct {

	Id string `json:"id,omitempty"`

	Url string `json:"url,omitempty"`

	HtmlUrl string `json:"html_url,omitempty"`

	DiffUrl string `json:"diff_url,omitempty"`

	PatchUrl string `json:"patch_url,omitempty"`

	IssueUrl string `json:"issue_url,omitempty"`

	CommitsUrl string `json:"commits_url,omitempty"`

	ReviewCommentsUrl string `json:"review_comments_url,omitempty"`

	ReviewCommentUrl string `json:"review_comment_url,omitempty"`

	CommentsUrl string `json:"comments_url,omitempty"`

	StatusesUrl string `json:"statuses_url,omitempty"`

	Number string `json:"number,omitempty"`

	State string `json:"state,omitempty"`

	Title string `json:"title,omitempty"`

	Body string `json:"body,omitempty"`

	Assignee string `json:"assignee,omitempty"`

	Milestone string `json:"milestone,omitempty"`

	Locked string `json:"locked,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	ClosedAt string `json:"closed_at,omitempty"`

	MergedAt string `json:"merged_at,omitempty"`

	Head string `json:"head,omitempty"`

	Base string `json:"base,omitempty"`

	Links string `json:"_links,omitempty"`

	User string `json:"user,omitempty"`
}
